const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    context: path.resolve(__dirname, './resources/assets'),
    entry: {
        app: './js/app',
    },
    output: {
        path: path.resolve(__dirname, "public/static/build"),
        filename: "[name].js",
        publicPath: '/static/build',
        library: "rApp",
    },
    resolve: {
        modules: [
            "node_modules",
            path.resolve(__dirname, "resources/assets"),
        ],
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [
                    path.resolve(__dirname, "node_modules")
                ],
                use: ['babel-loader'],
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                include: [
                    path.resolve(__dirname, "resources/assets/components")
                ],
                options: {
                    loaders: {
                        scss: 'vue-style-loader!css-loader!sass-loader'
                    }
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-sprite-loader',
                options: {}
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                                minimize: true,
                                sourceMap: true
                            }
                        }
                    ]
                }),
            },
            {
                test: /\.scss$|\.sass$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                                minimize: true,
                                sourceMap: true
                            }
                        },
                        {
                            loader: "sass-loader",
                        },
                    ]
                }),
            },
        ],
    },
    plugins: [
        new ExtractTextPlugin("styles.css"),
    ],
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9004
    },
    devtool: "source-map",
};