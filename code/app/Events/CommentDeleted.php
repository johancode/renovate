<?php

namespace App\Events;

use App\Models\Comment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CommentDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $commentId;
    public $threadId;
    public $companyId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->commentId = $comment->id;
        $this->threadId = $comment->thread ? $comment->thread->id : null;

        $table = @get_class($comment->thread->commentable);
        if ($table == \App\Models\Company::class) {
            $this->companyId = $comment->thread->commentable->id;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
