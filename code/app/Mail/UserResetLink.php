<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserResetLink extends Mailable
{
    use Queueable, SerializesModels;

    protected $token;

    public function __construct($token)
    {
        $this->token = url(route('password.reset', $token));
    }

    public function build()
    {
        return $this
            ->subject("Изменение пароля для аккаунта")
            ->view('emails.auth.user_reset_password')
            ->with(['token' => $this->token]);
    }
}
