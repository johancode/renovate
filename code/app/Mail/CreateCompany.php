<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateCompany extends Mailable
{
    use Queueable, SerializesModels;

    protected $companyData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($companyData)
    {
        $this->companyData = $companyData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $text = "";

        $text .= "Название: " . ($this->companyData['name'] ?? "не указано") . "<br>";
        $text .= "Специализация" . ($this->companyData['specialization'] ?? "не указано") . "<br>";
        $text .= "Информация:<br>" . ($this->companyData['info'] ?? "не указано");


        return $this
            ->subject("Новая компания на сайте")
            ->view('emails.admin.new-company')
            ->with([
                "text" => $text
            ]);

//        return $this->view('view.name');
    }
}
