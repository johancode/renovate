<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivationLink extends Mailable
{
    use Queueable, SerializesModels;

    protected $activationHash;
    protected $link;

    public function __construct($activationHash)
    {
        $this->link = route('user_activation', $activationHash);
    }

    public function build()
    {
        return $this
            ->subject("Регистрация на сайте")
            ->view('emails.auth.user_activation')
            ->with(['link' => $this->link]);
    }
}
