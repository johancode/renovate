<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWasGenerated extends Mailable
{
    use Queueable, SerializesModels;

    protected $password;
    protected $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password, $email)
    {
        $this->password = $password;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Регистрация на сайте")
            ->view('emails.auth.user_generated')
            ->with([
                'userEmail' => $this->email,
                'userPass' => $this->password,
                'loginLink' => route('login'),
            ]);
    }
}
