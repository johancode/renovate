<?php

namespace App\Helpers;

use App\Models\CompanyCategory;
use App\Models\TextBlock;
use Illuminate\Database\Eloquent\Collection;
use phpDocumentor\Reflection\Types\Integer;
use Carbon\Carbon;

class ContentHelper
{
    private $textBlocksCache = null;

    function __construct()
    {
        $this->textBlocksCache = TextBlock::get()
            ->mapWithKeys(function ($item) {
                return [$item['key'] => $item['value']];
            })
            ->toArray();
    }

    function getText($key)
    {
        if (!isset($this->textBlocksCache[$key])) {
            $newBlock = TextBlock::create([
                "key" => $key,
            ]);
            $this->textBlocksCache[$key] = "";
        }

        return $this->textBlocksCache[$key];
    }

    function prepareUserText($text)
    {
        $text = str_replace('\n', "<br>", $text);

        $text = strip_tags($text, "<br><ul><li>");
        $text = preg_replace('~(?:<br\b[^>]*>|\R){2,}~iu', "</p><p>", $text);

        $text = "<p>" . $text . "</p>";

        return $text;
    }

    function preparePhoneNumber($number)
    {
        $number = preg_replace("~[^\d\+]+~", "", $number);
        return $number;
    }

    function encodePhone($number)
    {
        $number = $this->preparePhoneNumber($number);
//        $number = implode("a", str_split($number));
        return $number;
    }

    function prepareLink($text)
    {
        if (strpos($text, "http") === false) {
            $text = "http://" . $text;
        }

        return $text;
    }

    function declension($num, $zero, $one, $two, $many)
    {
        $nmod10 = $num % 10;
        $nmod100 = $num % 100;

        if (!$num) {
            return $zero;
        }

        if (($num == 1) || ($nmod10 == 1 && $nmod100 != 11)) {
            return $one;
        }

        if ($nmod10 > 1 && $nmod10 < 5 && $nmod100 != 12 && $nmod100 != 13 && $nmod100 != 14) {
            return $two;
        }

        return $many;
    }

    function prepareDate(Carbon $date = null, $showTime = false)
    {
        if (!$date) {
            return false;
        }

        $string = "";
        $MONTH = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];


        $timestamp = $date->timestamp;

        if ((strtotime("00:00:00") < $timestamp) && (strtotime("23:59:59") > $timestamp)) {
            //сегодня
            $string .= "Сегодня";
        } else if ((strtotime("00:00:00 -1day") < $timestamp) && (strtotime("23:59:59 -1day") > $timestamp)) {
            //вчера
            $string .= "Вчера";
        } else {
            //дата
            $string .= $date->day * 1;
            $string .= " ";
            $string .= $MONTH[$date->month];
            $string .= " ";
            $string .= $date->year;
        }

        if ($showTime) {
            $string .= ", ";
            $string .= $date->hour . ":" . $date->minute;
        }

        return $string;
    }
}