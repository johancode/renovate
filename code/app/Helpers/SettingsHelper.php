<?php

namespace App\Helpers;

use App\Models\Setting;
use Schema;

class SettingsHelper
{
    private $settingsCache = null;


    function __construct()
    {
        if (Schema::hasTable("settings")) {
            $this->settingsCache = Setting::get()
                ->mapWithKeys(function ($item) {
                    return [$item['key'] => $item['value']];
                })
                ->toArray();
        }
    }

    function get($key)
    {
        return $this->settingsCache[$key] ?? null;
    }
}