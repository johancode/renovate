<?php

namespace App\Providers;

use View;
use App\Helpers\SettingsHelper;
use App\Helpers\ContentHelper;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->singleton('ContentHelper', function ($app) {
            return new ContentHelper($app->request);
        });

        $this->app->singleton('SettingsHelper', function ($app) {
            return new SettingsHelper($app->request);
        });


        View::composer('*', \App\Http\ViewComposers\SiteComposer::class);
        View::composer('profile/*', \App\Http\ViewComposers\ProfileComposer::class);
    }

    public function register()
    {
        //
    }
}
