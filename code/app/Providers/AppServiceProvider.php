<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Blade;
use App;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('SECURE_CONNECTION', false)) {
            URL::forceScheme('https');
        }


        Blade::directive('bronly', function ($expression) {
            return "<?php echo strip_tags(nl2br($expression), '<br>') ?>";
        });

        App::setLocale("ru");
        Carbon::setLocale(config('app.locale'));

        \Debugbar::disable();

        config([
            'seotools.opengraph.defaults.images' => [url("static/images/logo.png")],
            'seotools.opengraph.defaults.site_name' => config('app.site_name'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
