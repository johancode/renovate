<?php

namespace App\Providers;

use App\Repositories\Company\CompaniesElasticSearchRepository;
use App\Repositories\Company\CompaniesRepository;
use App\Repositories\CompanyCategory\CompanyCategoryRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CompanyCategoryRepository::class, function () {
            return new CompanyCategoryRepository();
        });

        $this->app->bind(CompaniesRepository::class, function () {
            // TODO: for testing ENV - replace to EloquentRepo

            return new CompaniesElasticSearchRepository();
        });
    }
}
