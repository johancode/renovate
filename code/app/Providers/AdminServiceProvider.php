<?php

namespace App\Providers;

use Route;
use App;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // override backpack middleware
        Route::aliasMiddleware('admin', \App\Http\Middleware\AuthenticateAdmin::class);

        // set panel localization
//        App::setLocale('ru');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
