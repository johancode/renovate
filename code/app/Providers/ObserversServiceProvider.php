<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\CompanyMasterCategory;
use App\Models\Observers\CommentObserver;
use App\Models\Observers\CompanyMasterCategoryObserver;
use App\Models\Observers\PageObserver;
use App\Models\Observers\UserUploadImageObserver;
use App\Models\Page;
use App\Models\UserUploadImage;
use App\User;
use App\Models\Company;
use App\Models\CompanyService;
use App\Models\CompanyCategory;
use App\Models\Observers\CompanyObserver;
use App\Models\Observers\CompanyServiceObserver;
use App\Models\Observers\CompanyCategoryObserver;
use App\Models\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class ObserversServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Company::observe(CompanyObserver::class);
        CompanyService::observe(CompanyServiceObserver::class);
        CompanyCategory::observe(CompanyCategoryObserver::class);
        CompanyMasterCategory::observe(CompanyMasterCategoryObserver::class);
        User::observe(UserObserver::class);
        UserUploadImage::observe(UserUploadImageObserver::class);
        Page::observe(PageObserver::class);
        Comment::observe(CommentObserver::class);
    }

    public function register()
    {
    }
}
