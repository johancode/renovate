<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Company;
use DB;

class CompanyEventSubscriber implements ShouldQueue
{
    public function onCompanySaved($event)
    {
        $company = $event->company;
        $this->updateCompanyPositionScore($company);
    }

    public function onCommentCreated($event)
    {
        if ($event->company) {
            $this->updateCompanyPositionScore($event->company);
        }
    }

    public function onCommentDeleted($event)
    {
        if ($event->companyId !== null) {
            $company = Company::whereId($event->companyId)->first();
            $this->updateCompanyPositionScore($company);
        }
    }

    public function subscribe($events)
    {
        $events->listen(
            \App\Events\CommentCreated::class,
            'App\Listeners\CompanyEventSubscriber@onCommentCreated'
        );

        $events->listen(
            \App\Events\CommentDeleted::class,
            'App\Listeners\CompanyEventSubscriber@onCommentDeleted'
        );

        $events->listen(
            \App\Events\CompanySaved::class,
            'App\Listeners\CompanyEventSubscriber@onCompanySaved'
        );
    }

    private function updateCompanyPositionScore(Company $company)
    {
        $score = $this->getCompanyScore($company);

        DB::table('companies')
            ->where('id', $company->id)
            ->update(['position_score' => $score]);
    }

    /**
     * @param Company $company
     * @return int score
     *
     * 1-5 фото         + 10
     * 5-15 фото        + 20
     * 16-20 фото       + 30
     *
     * специализация    + 10
     * информация       + 20
     * отзывы           + 20
     * контакты         + 10
     * адрес            + 5
     * услуги           + 30
     * рейтинг          + 1 x rate
     */
    private function getCompanyScore(Company $company)
    {
        $score = 0;

        if ($company->images) {
            if (count($company->images) < 6) {
                $score += 10;
            } else if (6 < count($company->images) && count($company->images) < 16) {
                $score += 20;
            } else if (16 < count($company->images)) {
                $score += 30;
            }
        }


        if ($company->specialization) {
            $score += 10;
        }


        if ($company->info && ($company->info != "") && ($company->info != "<p></p>")) {
            $score += 10;
        }

        if ($company->info && ($company->info != "") && ($company->info != "<p></p>") && (mb_strlen(strip_tags($company->info)) > 250)) {
            $score += 20;
        }


        $thread = $company->getThread();
        $commentsCount = $thread
            ->comments()
            ->count();

        if ($commentsCount) {
            $score += 30;
        }


        $contacts = $company->getContacts();
        if ($contacts['site'] || $contacts['email'] || $contacts['phone']) {
            $score += 10;
        }


        if ($company->addresses) {
            $score += 5;
        }


        $servicesCount = $company
            ->services()
            ->active()
            ->count();

        if ($servicesCount) {
            $score += 30;
        }


//        if ($company->rate > 0) {
//            $score += $company->rate * 1;
//        }


        return $score;
    }

}