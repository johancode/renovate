<?php

namespace App\Repositories\CompanyCategory;

use App\Models\CompanyCategory;
use App\Models\CompanyMasterCategory;
use Illuminate\Database\Eloquent\Collection;

class CompanyCategoriesRepository
{
    private $typeFilter = null;
    private $typeFilterAllow = ["is_shop", "is_service"];

    /**
     * Get tree of company categories list
     * @return Collection
     */
    function getTree()
    {
        $categories = CompanyCategory::active();


        if ($this->typeFilter) {
            $categories = $categories->where($this->typeFilter, true);
        }


        $categories = $categories->orderBy('name')
            ->get()
            ->mapToGroups(function ($item, $key) {
                return [$item->masterCategory->id => $item];
            });

        $categoriesList = CompanyMasterCategory::orderBy('lft')
            ->get()
            ->mapWithKeys(function ($item) use ($categories) {
                return [$item->id => (object)[
                    'id' => $item->id,
                    'name' => $item->name,
                    'icon_label' => $item->icon_label,
                    'companies_score' => $item->companies_score,
                    'categories' => ($categories[$item->id] ?? [])
                ]];
            })
            ->filter(function ($item) {
                return count($item->categories);
            });

        return $categoriesList;
    }

    /**
     * Set filter by type field
     *
     * @param null $typeFilter
     */
    public function setTypeFilter($typeFilter)
    {
        if (in_array($typeFilter, $this->typeFilterAllow)) {
            $this->typeFilter = $typeFilter;
        }
    }
}