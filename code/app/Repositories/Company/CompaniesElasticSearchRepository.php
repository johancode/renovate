<?php

namespace App\Repositories\Company;

use App;
use App\Repositories\ElasticBaseRepository;

class CompaniesElasticSearchRepository extends ElasticBaseRepository implements CompaniesRepository
{
    protected $model = App\Models\Company::class;

    public function getESSearchFields()
    {
        return [
            'name^3',
            'info^5',
            'specialization^4',
//            'categories^4',
//                            'contacts.value',
//                            'addresses.address'
        ];
    }

    public function getFiltersTypes()
    {
        return [
            "images" => [
                "type" => "checkbox",
                "filter" => [
                    "type" => "range",
                    "value" => [
                        "images_count" => [
                            "gt" => 0
                        ]
                    ]
                ]
            ],
            "comments" => [
                "type" => "checkbox",
                "filter" => [
                    "type" => "range",
                    "value" => [
                        "comments_count" => [
                            "gt" => 0
                        ]
                    ]
                ]
            ],
            "services" => [
                "type" => "checkbox",
                "filter" => [
                    "type" => "range",
                    "value" => [
                        "services_count" => [
                            "gt" => 0
                        ]
                    ]
                ]
            ],
            "category" => [
                "type" => "multi-checkbox",
                "filter" => [
                    "type" => "nested",
                    "value" => function ($filterValue) {
                        return [
                            'path' => 'categories',
                            'query' => [
                                ['term' => ["categories.id" => $filterValue]],
                            ]
                        ];
                    }
                ]
            ],

        ];
    }
}