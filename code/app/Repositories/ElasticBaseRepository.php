<?php

namespace App\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;
use App;

abstract class ElasticBaseRepository
{
    private $order;
    private $searchQuery;
    private $currentPage = 1;
    private $perPageAmount = 10;
    private $totalAmount = 0;
    private $filterValues = [];
    private $elastic;

    /**
     * ElasticBaseRepository constructor.
     */
    public function __construct()
    {
        $this->elastic = App::make(\Elasticsearch\Client::class);
        $mapName = collect(config('elasticsearch.mappings'))
            ->filter(function ($value, $key) {
                return $key == $this->model;
            })
            ->first();

        $this->map = new $mapName;
    }

    /**
     * Set search string value
     *
     * @param string $query
     * @return ElasticBaseRepository
     */
    public function setSearchQuery(string $query)
    {
        $this->searchQuery = $query;
        return $this;
    }


    /**
     * Final method, return pagination object
     *
     * @return LengthAwarePaginator
     */
    public function get(): LengthAwarePaginator
    {
        $ESResponse = $this->getESResponse($this->buildESRequest());
        $this->totalAmount = $ESResponse['hits']['total'];
        $items = $this->buildEloquentModels(array_pluck($ESResponse['hits']['hits'], '_source') ?: []);

        $paginatorOptions = [
            'path' => url(request()->path()),
        ];

        return new LengthAwarePaginator($items, $this->totalAmount, $this->perPageAmount, $this->currentPage, $paginatorOptions);
    }

    /**
     * Set the order type
     *
     * @param mixed $order
     * @return ElasticBaseRepository
     */
    public function setOrder($order = null)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Set the current paginate page
     *
     * @param int $currentPage
     * @return ElasticBaseRepository
     */
    public function setCurrentPage(int $currentPage = null)
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    /**
     * Set per page pagination amount
     *
     * @param int $perPageAmount
     * @return ElasticBaseRepository
     */
    public function setPerPageAmount(int $perPageAmount)
    {
        $this->perPageAmount = $perPageAmount;
        return $this;
    }

    /**
     * Set the filter value
     *
     * @param array $filterValues
     * @return ElasticBaseRepository
     */
    public function setFilters(array $filterValues)
    {
        $types = $this->getFiltersTypes();

        foreach ($filterValues as $filterName => $filterValue) {
            if (array_key_exists($filterName, $types)) {
                switch ($types[$filterName]['type']) {
                    case "checkbox":
                        if ($filterValue == "true") {
                            $this->filterValues[$filterName] = true;
                        }
                        break;

                    case "multi-checkbox":
                        $this->filterValues[$filterName] = $filterValue;
                        break;
                }
            }
        }

        return $this;
    }

    /**
     * Call ES and get response
     *
     * @return array
     */
    private function buildESRequest(): array
    {
        // BASE TEMPLATE
        $result = [
            'index' => $this->map->getSearchIndex(),
            'type' => $this->map->getSearchType(),
            'size' => $this->perPageAmount,
            'body' => [
                'size' => $this->perPageAmount,
                'from' => $this->currentPage ? (($this->currentPage - 1) * $this->perPageAmount) : 0,
            ],
        ];


        // SEARCH QUERY
        $ESSearchParams = ['match_all' => (object)[]];
        if ($this->searchQuery) {
            $ESSearchParams = [
                'multi_match' => [
                    'fields' => $this->getESSearchFields(),
                    'query' => $this->searchQuery,
                ],
            ];
        }

        $result['body']['query'] = ['bool' => ['must' => [$ESSearchParams]]];


        // SORTING
        if ($this->order) {
            // set order by user select
        } else if ($this->searchQuery) {
            // default search ordering by "_score"
        } else {
            // default ordering by own index
            $result['body']['sort'] = ['position_index' => 'asc'];
        }
        $result['body']['sort'] = ['position_index' => 'asc'];


        // FILTERS
        if ($this->filterValues) {
            $result['body']['query']['bool']['filter'][] = $this->getESFilters();
        }


        // TODO - move it into child class
        // Active only
        $result['body']['query']['bool']['filter'][] = ['term' => ["active" => true]];


        // DQL TEMPLATE
//        $result['body']['query'] = [
//            'bool' => [
//                'must' => [
////                    ['range' => ['images_count' => ["gt" => 0]]],
//                    ['match_all' => (object)[]],
////                    ['multi_match' => [
////                        'fields' => $this->getESSearchFields(),
////                        'query' => $this->searchQuery,
////                    ]],
//
//                ],
//                'filter' => [
//                    [
//                        'nested' => [
//                            'path' => 'categories',
//                            'query' => [
//                                ['term' => ["categories.id" => 123]],
//                            ]
//                        ]
//                    ],
//                    ['term' => ["active" => true]],
//
////                    ['term' => ["categories.id" => 133]],
////                    ['range' => ['images_count' => ["gt" => 0]]],
////                    ['range' => ['comments_count' => ["gt" => 0]]],
////                    ['range' => ['services_count' => ["gt" => 0]]],
//                ],
//            ],
//        ];

//        dd($result);


        return $result;
    }

    private function getESResponse(array $request): array
    {
        return $this->elastic->search($request);
    }

    /**
     * Build Eloquent models
     *
     * @param array $ESItems
     * @return \Illuminate\Support\Collection
     */
    private function buildEloquentModels(array $ESItems = [])
    {
        $items = collect();

        if ($ESItems) {
            $ids = collect($ESItems)->pluck("id")->toArray();
            $ESItems = collect($ESItems)
                ->mapWithKeys(function ($item) {
                    return [$item['id'] => $item];
                })
                ->toArray();

            $eloquentModel = new $this->model;
            $items = $eloquentModel->whereIn("id", $ids)
                ->get()
                ->sortBy(function ($item) use ($ids) {
                    return array_search($item->id, $ids);
                })
                ->map(function ($item) use ($ESItems) {
                    $item = $this->appendESProperties($item, $ESItems[$item['id']]);
                    return $item;
                });
        }

        return $items->values();
    }

    /**
     * Append properties from ES
     *
     * @param $eqItem
     * @param $esItem
     * @return mixed
     */
    private function appendESProperties($eqItem, $esItem)
    {
        $attachedFields = $this->map->getESAttachedFields();

        foreach ($attachedFields as $attachedField) {
            $eqItem->{$attachedField} = $esItem[$attachedField];
        }

        return $eqItem;
    }

    /**
     * Get filters as array ES param
     *
     * @return array
     */
    private function getESFilters()
    {
        $filterList = [];
        $filterTypes = $this->getFiltersTypes();

        foreach ($this->filterValues as $filterName => $filterValue) {
            $value = $filterTypes[$filterName]['filter']['value'];
            if (is_callable($value)) {
                $value = $value($filterValue);
            }

            $filterList[] = [$filterTypes[$filterName]['filter']['type'] => $value];
        }

        return $filterList;
    }
}