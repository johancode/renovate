<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('website:generate-footer-links')->dailyAt("02:00");
        $schedule->command('website:update-master-category-counts')->dailyAt("02:10");
        $schedule->command('website:update-companies-position-index')->dailyAt("02:30");
        $schedule->command('upload:clear-user-files')->dailyAt("03:00");

//        $schedule->command('website:generate-sitemap')->daily(); // run this job as renovate user

//        $schedule->command('website:grab-flamp-comments')->everyFiveMinutes();
//        $schedule->command('website:resave-images')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
