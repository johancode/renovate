<?php

namespace App\Console\Commands;

use Elasticsearch\Client;
use Illuminate\Console\Command;

class ElasticSearchCreateMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:create-map {mapName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new search map';

    /**
     * Elastic Search client
     *
     * @var \Elasticsearch\Client
     */
    protected $ESClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->ESClient = $client;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Creating new search index");

        $mapName = '\\App\\ElasticSearch\\Mappings\\' . $this->argument('mapName');
        $map = new $mapName();

        if ($this->ESClient->indices()->exists(['index' => $map->getSearchIndex()])) {
            $this->ESClient->indices()->delete(['index' => $map->getSearchIndex()]);
            $this->info("\nRemove old index");
        }

        $this->info("\nCreate new index");
        $result = $this->ESClient->indices()->create($map->getMapSettings());

        $this->info("\nDone!");

        $this->info("\nElastic search response:");
        dump($result);
    }
}
