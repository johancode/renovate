<?php

namespace App\Console\Commands;

use App\Models\CompanyCategory;
use App\Models\TextBlock;
use Illuminate\Console\Command;

class GenerateFooterCategoriesLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:generate-footer-links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate footer links - top categories of company';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companiesList = CompanyCategory::active()
            ->withCount('companies')
            ->orderBy('companies_count', 'desc')
            ->limit(8)
            ->get()
            ->chunk(4);


        foreach ($companiesList as $num => $companies) {
            $textBlock = TextBlock::firstOrCreate([
                'key' => 'footer: links block #' . ($num + 1),
                'is_html' => true,
            ]);


            $text = "<ul>";
            foreach ($companies as $company) {
                $text .= "<li><a href='{$company->link}'>{$company->name}</a></li>";
            }
            $text .= "</ul>";


            $textBlock->value = $text;
            $textBlock->save();
        }

        echo "done\n";
    }
}
