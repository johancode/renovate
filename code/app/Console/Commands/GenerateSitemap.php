<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\CompanyService;
use Illuminate\Console\Command;
use App;
use DB;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:generate-sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sitemap file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = App::make('sitemap');
        $sitemapCounter = 0;


        // categories --------------->
        $categories = CompanyCategory::active()
            ->orderBy('name')
            ->get();


        $sitemap->add(route('companies.list'), null, 0.8, "monthly");

        foreach ($categories as $category) {
            $sitemap->add($category->link, $category->updated_at, 0.8, "monthly");
        }

        $sitemap->store('xml', 'sitemap-' . $sitemapCounter);
        $sitemap->addSitemap(secure_url('sitemap-' . $sitemapCounter . '.xml'));
        $sitemap->model->resetItems();

        $sitemapCounter++;
        // categories ---------------<


        // sevices --------------->
        $services = CompanyService::active()
            ->orderBy('name')
            ->get();

        $sitemap->add(route('companies.services'), null, 0.8, "monthly");

        foreach ($services as $service) {
            $sitemap->add($service->link, $service->updated_at, 0.8, "monthly");
        }

        $sitemap->store('xml', 'sitemap-' . $sitemapCounter);
        $sitemap->addSitemap(secure_url('sitemap-' . $sitemapCounter . '.xml'));
        $sitemap->model->resetItems();

        $sitemapCounter++;
        // sevices ---------------<


        $companies = Company::active()
            ->orderBy('updated_at', 'desc')
            ->get();

        // add every product to multiple sitemaps with one sitemap index
        $counter = 0;
        foreach ($companies as $company) {
            if ($counter == 1000) {
                // generate new sitemap file
                $sitemap->store('xml', 'sitemap-' . $sitemapCounter);
                // add the file to the sitemaps array
                $sitemap->addSitemap(secure_url('sitemap-' . $sitemapCounter . '.xml'));
                // reset items array (clear memory)
                $sitemap->model->resetItems();
                // reset the counter
                $counter = 0;
                // count generated sitemap
                $sitemapCounter++;
            }

            
            $images = null;
            if ($company->images) {
                $images = [];

                foreach ($company->images as $image) {
                    $images[] = [
                        'url' => $image->modal,
                        'title' => $company->name,
                    ];
                }
            }


            // add product to items array
            $sitemap->add($company->link, $company->updated_at, 0.5, "monthly", $images ?? null);
            // count number of elements
            $counter++;
        }

        // you need to check for unused items
        if (!empty($sitemap->model->getItems())) {
            // generate sitemap with last items
            $sitemap->store('xml', 'sitemap-' . $sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(secure_url('sitemap-' . $sitemapCounter . '.xml'));
            // reset items array
            $sitemap->model->resetItems();
        }

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->store('sitemapindex', 'sitemap');
    }
}
