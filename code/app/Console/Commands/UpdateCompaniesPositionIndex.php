<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use DB;
use App;

class UpdateCompaniesPositionIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:update-companies-position-index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update «position_index» for companies based on «position_score»';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = DB::table("companies")
            ->select("id", "name", "position_index", "position_score", "moderated")
            ->orderBy('moderated', 'desc')
            ->orderBy('position_score', 'desc')
            ->inRandomOrder();

        foreach ($companies->cursor() as $index => $company) {
            DB::table('companies')
                ->where('id', $company->id)
                ->update(['position_index' => $index]);

            $this->output->write('.');
        }

        echo "DONE\n";
    }
}
