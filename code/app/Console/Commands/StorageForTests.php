<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class StorageForTests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:link-for-test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "public/storage-test" to "storage/app/public_for_test"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(public_path('storage-test'))) {
            return $this->error('The "public/storage-test" directory already exists.');
        }

        $this->laravel->make('files')->link(
            storage_path('app/public_for_test'), public_path('storage-test')
        );

        $this->info('The [public/storage-test] directory has been linked.');
    }
}
