<?php

namespace App\Console\Commands;

use File;
use DB;
use Illuminate\Console\Command;
use JohanCode\BackpackImageUploader\Models\TempImage;

class ResaveImages extends Command
{
    private $maxStep = 10;
    private $modelsFields = [
        \App\Models\Company::class => ['images'],
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:resave-images {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resave images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('reset')) {
            foreach ($this->modelsFields as $modelName => $fields) {
                $tableName = (new $modelName)->getTable();
                DB::table($tableName)->update(['resaved' => false]);
            }

            echo("reset field 'resaved' done\n");
            return true;
        }


        $doneModels = [];
        $models = array_keys($this->modelsFields);

        echo("start resaving\n");


        while ($this->maxStep) {
            $item = null;

            foreach ($models as $modelName) {
                if (in_array($modelName, $doneModels)) {
                    continue;
                }

                $item = $modelName::where('resaved', false)->first();

                if ($item) {
                    break;
                } else {
                    $doneModels[] = $modelName;
                }
            }


            if ($item) {
                $removeTempImages = [];
                echo get_class($item) . " => " . $item->id . "\n";

                foreach ($this->modelsFields[$modelName] as $field) {
                    if ($item->{$field}) {
                        //single
                        if (isset($item->{$field}->original)) {
                            $tempImage = TempImage::create([
                                'file' => $item->{$field}->original
                            ]);

                            $item->removeImages($field);
                            $item->{$field} = $tempImage->file->original;

                            $removeTempImages[] = $tempImage;
                        }

                        //multiple
                        if (isset($item->{$field}[0]->original)) {
                            $images = [];
                            foreach ($item->{$field} as $img) {
                                $tempImage = TempImage::create([
                                    'file' => $img->original
                                ]);

                                $images[] = $tempImage->file->original;
                                $removeTempImages[] = $tempImage;
                            }

                            $item->removeImages($field);
                            $item->{$field} = $images;
                        }
                    }
                }

                $item->resaved = true;
                $item->save();


                foreach ($removeTempImages as $tempImage) {
                    $tempImage->delete();
                }

                $this->maxStep--;
            } else {
                break;
            }
        }


        echo("resaved done\n\n");


        foreach ($models as $modelName) {
            $lessItems = $modelName::where('resaved', false)->count();
            echo "table '" . $modelName . "'" . " need resave items: " . $lessItems . "\n";
        }
    }
}
