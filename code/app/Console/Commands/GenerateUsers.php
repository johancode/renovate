<?php

namespace App\Console\Commands;

use App\Mail\UserWasGenerated;
use App\Models\Company;
use App\User;
use Illuminate\Console\Command;
use DB;
use Mail;
use Hash;

class GenerateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:generate-users {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new users by companies contacts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('reset')) {
            DB::table("companies")->update(['resaved' => false]);
            echo("reset field 'resaved' done\n");
            return true;
        }

        $companies = Company::where('resaved', false)
            ->take(400)
            ->get();

        foreach ($companies as $company) {
            $email = collect($company->contacts)
                ->where('type', 'email')
                ->pluck("value")
                ->first();


            if ($email && !$company->user) {
                $pass = str_random(5);

                try {
                    $newUser = User::create([
                        "name" => $company->name,
                        "email" => $email,
                        "active" => true,
                        "password" => bcrypt($pass),
                    ]);
                } catch (\Exception $e) {
                    dump($e->getMessage());
                    break;
                }

                $company->user_id = $newUser->id;


                Mail::to($newUser)->queue(new UserWasGenerated($pass, $email));
                dump($email);
            }


            $company->resaved = true;
            $company->save();
        }


        $lessItems = Company::where('resaved', false)->count();
        echo "------\n";
        echo "less items: " . $lessItems . "\n";
    }
}
