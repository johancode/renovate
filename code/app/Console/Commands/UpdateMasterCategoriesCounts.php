<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyMasterCategory;
use Illuminate\Console\Command;

class UpdateMasterCategoriesCounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:update-master-category-counts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update master category counts of company';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $masterCategories = CompanyMasterCategory::get();
        foreach ($masterCategories as $masterCategory) {
            $companiesCount = Company::where('active', true)
                ->whereHas('categories', function ($query) use ($masterCategory) {
                    $query->where('master_category_id', $masterCategory->id);
                })
                ->count();

            $masterCategory->companies_score = $companiesCount;
            $masterCategory->save();
        }
    }
}
