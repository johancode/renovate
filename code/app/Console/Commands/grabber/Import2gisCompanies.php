<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\GrabberRecord;
use Illuminate\Console\Command;
use Storage;
use DB;

class Import2GisCompanies extends Command
{
    protected $signature = 'grabber:2gis.import';
    protected $description = 'import grabbed data from 2gis';

    private $diskName = "public";
    private $tempFilesDir = "grabber";
    private $categoriesMap = [];

    public function __construct()
    {
        parent::__construct();
        Storage::disk($this->diskName)->makeDirectory($this->tempFilesDir);
    }


    public function handle()
    {
//        DB::table("ref_companies_categories")->truncate();
//        DB::table("companies")->truncate();
//        DB::table("company_categories")->truncate();

        if (!CompanyCategory::first()) {
            $this->grabCategories();
        }

        $this->loadCategoriesMap();
        $this->processGrabRecords(20);
        $this->clearTempData();
    }

    private function clearTempData()
    {
        Storage::disk($this->diskName)->deleteDirectory($this->tempFilesDir);
    }

    private function processGrabRecords($limit)
    {
        $records = GrabberRecord::where([
            ['status', 'to_export'],
            ['source', '2gis']
        ])
            ->take($limit)
//            ->orderBy('id', 'desc')
            ->inRandomOrder()
            ->get();


        if (!$records) {
            exit ("no items to import");
        }


        foreach ($records as $record) {
            $categories = [];

            if (!empty($record->content['rubrics'])) {
                foreach ($record->content['rubrics'] as $rubric) {
                    if (isset($this->categoriesMap[$rubric['name']])) {
                        $categories[] = $this->categoriesMap[$rubric['name']];
                    }
                }
            }


            $companyData = $record->content;
            $companyData['categories'] = $categories;

            $this->importNewCompany($companyData);

            $record->status = "to_remove";
            $record->save();

            sleep(rand(1, 3));
        }
    }


    private function importNewCompany($data)
    {
        $newCompanyName = $this->prepareCompanyName($data['name_ex']['primary']);
        $company = $this->findCompanyByName($newCompanyName);


        if ($company && $this->companyAddressExist($company, $data['address'])) {
            return false;
        }


        $newAddress = [
            'address' => $data['address'],
            'coordinates' => $data['coordinates'],
            'info' => @$data['name_ex']['description'],
        ];

        if ($company) {
            if (is_array($company->addresses)) {
                $addresses = array_merge($company->addresses, [$newAddress]);
                $company->addresses = $addresses;
            } else {
                $company->addresses = [$newAddress];
            }

            $company->save();

            return true;
        }


        $company = Company::create([
            'source' => 1,
            'name' => $this->prepareCompanyName($newCompanyName)
        ]);

        $company->active = true;
        $company->specialization = empty($data['name_ex']['extension']) ? null : $data['name_ex']['extension'];
        $company->schedule = json_encode($data['schedule']);
        $company->addresses = [$newAddress];


        if (!empty($data['contacts'])) {
            $contacts = [];

            foreach ($data['contacts'] as $contact) {
                $type = 'site';

                if (in_array($contact['type'], ['phone', 'email'])) {
                    $type = $contact['type'];
                }

                $contacts[] = [
                    'type' => $type,
                    'value' => $contact['text'],
                ];
            }
        }

        $company->contacts = $contacts;


        if (!empty($data['images'])) {
            $imagesUriList = $data['images'];
            $localImagesUriList = [];

            foreach ($imagesUriList as $uri) {
                $fileName = uniqid() . ".jpg";
                $newFilePath = public_path(Storage::disk($this->diskName)->url($this->tempFilesDir . "/" . $fileName));
                @file_put_contents($newFilePath, fopen($uri, 'r'));

                if (substr(mime_content_type($newFilePath), 0, 5) == 'image') {
                    $localImagesUriList[] = $newFilePath;
                }
            }

            $company->images = $localImagesUriList;
        }

        if ($data['categories']) {
            $company->categories()->sync($data['categories']);
        }


        $company->save();

        return true;
    }


    private function prepareCompanyName($text)
    {
        $text = str_replace(", ООО", "", $text);
        return $text;
    }

    private function findCompanyByName($name)
    {
        return Company::where(['name' => $name, 'source' => 1])->first();
    }

    private function companyAddressExist($company, $address)
    {
        if (!empty($company->addresses)) {
            foreach ($company->addresses as $map_point) {
                if ($map_point['address'] == $address) {
                    return true;
                }
            }
        }

        return false;
    }

    private function grabCategories()
    {
        $createdCategoriesMap = [];

        $file = file_get_contents(resource_path('grabber/2gis/companies1.json'));
        $data = json_decode($file, true);

        foreach ($data as $categoryName => $companiesIds) {
            $category = CompanyCategory::create(['name' => $categoryName]);
            $createdCategoriesMap[$category->name] = $category->id;
        }


        $file = file_get_contents(resource_path('grabber/2gis/companies2.json'));
        $data = json_decode($file, true);

        foreach ($data as $categoryName => $companiesIds) {
            $category = CompanyCategory::create(['name' => $categoryName]);
            $createdCategoriesMap[$category->name] = $category->id;
        }

        file_put_contents(resource_path('grabber/2gis/categories_map.json'), json_encode($createdCategoriesMap));
    }

    private function loadCategoriesMap()
    {
        $data = json_decode(file_get_contents(resource_path('grabber/2gis/categories_map.json')));

        foreach ($data as $name => $id) {
            $this->categoriesMap[$name] = $id;
        }
    }

}
