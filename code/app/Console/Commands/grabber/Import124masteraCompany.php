<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyGrabRef;
use App\Models\CompanyService;
use App\Models\GrabberRecord;
use Illuminate\Console\Command;
use DB;
use Schema;
use Storage;
use Image;
use Mockery\Exception;

class Import124masteraCompany extends Command
{
    protected $signature = 'grabber:124mastera.import';
    protected $description = 'import grabbed data from 124mastera';

    private $availableServicesNames = [];
    private $diskName = "public";
    private $tempFilesDir = "grabber";

    public function __construct()
    {
        parent::__construct();
        Storage::disk($this->diskName)->makeDirectory($this->tempFilesDir);
    }


    public function handle()
    {
//        DB::table("ref_companies_services")->truncate();
//        DB::table("companies")->truncate();
//        DB::table("company_services")->truncate();


        $this->processGrabRecords(10);
        $this->clearTempData();
    }


    private function clearTempData()
    {
        Storage::disk($this->diskName)->deleteDirectory($this->tempFilesDir);
    }

    private function loadServices()
    {
        $this->availableServicesNames = CompanyService::pluck('id', 'name')->toArray();
    }

    private function processGrabRecords($limit)
    {
        $this->loadServices();

        $records = GrabberRecord::where([
            ['status', 'to_export'],
            ['source', '124mastera']
        ])
            ->take($limit)
            ->get();

        if (!$records) {
            exit ("no items to import");
        }


        foreach ($records as $record) {
            $services = [];

            foreach ($record->content['services'] as $service) {
                if (!isset($this->availableServicesNames[$service['title']])) {
                    $newService = CompanyService::create([
                        'name' => $service['title'],
                        'unit_type' => $service['unit']
                    ]);

                    $this->availableServicesNames[$newService->name] = $newService->id;
                }

                $services[$this->availableServicesNames[$service['title']]] = [
                    "price" => $service['price']
                ];
            }


            $companyData = $record->content;
            $companyData['services'] = $services;

            $this->importNewCompany($companyData);

            $record->status = "to_remove";
            $record->save();
        }


        echo "done\n";
    }


    private function importNewCompany($data)
    {
        if ($this->findCompanyByName($data['title'])) {
            return false;
        }

        if ($this->findCompanyByPhone($this->formatPhone($data['phone']) || $data['phone'])) {
            return false;
        }


        $company = Company::create([
            'name' => $data['title']
        ]);

        $company->active = true;
        $company->info = $this->formatHtmlText($data['company_info']);
        $company->phone = $this->formatPhone($data['phone']);


        $contacts = [];

        if (isset($data['site']) && $data['site']) {
            $site = $this->formatSiteAddress($data['site']);

            if ($site) {
                $contacts[] = [
                    'type' => 'site',
                    'value' => $site,
                ];
            }
        }

        $company->contacts = $contacts;


        if ($data['services']) {
            $company->services()->sync($data['services']);
        }


        if (!$company->phone) {
            $company->active = false;
            $company->phone = $data['phone'];
        }


        if (!empty($data['images']) || !empty($data['photos'])) {
            $imagesUriList = empty($data['images']) ? $data['photos'] : $data['images'];
            $localImagesUriList = [];

            foreach ($imagesUriList as $uri) {
                $fileName = uniqid() . ".jpg";
                $newFilePath = public_path(Storage::disk($this->diskName)->url($this->tempFilesDir . "/" . $fileName));

                file_put_contents($newFilePath, fopen($uri, 'r'));

                // remove watermarks
                $img = Image::make($newFilePath);
                $img->crop($img->width(), $img->height() - 85, 0, 0);
                $img->save($newFilePath);

                $localImagesUriList[] = $newFilePath;
            }

            $company->images = $localImagesUriList;
        }


        $company->save();

        return true;
    }


    private function findCompanyByName($name)
    {
        $company = Company::where('name', $name)->first();
        return $company;
    }

    private function findCompanyByPhone($phone)
    {
        $company = Company::where('phone', $phone)->first();
        return $company;
    }


    private function formatSiteAddress($dirtySiteLink)
    {
        $beautifySiteLink = $dirtySiteLink;

//        preg_match("/124mastera/", $dirty_site_link, $matches);
//        if (isset($matches[0])) {
//            return null;
//        }

        return $beautifySiteLink;
    }

    private function formatHtmlText($dirtyHtml)
    {
        $beautifyHtml = $dirtyHtml;

        $breaks = array("<br />", "<br>", "<br/>", "<br >");
        $beautifyHtml = str_ireplace($breaks, "\r\n", $beautifyHtml);
        $beautifyHtml = trim($beautifyHtml);
        $beautifyHtml = strip_tags($beautifyHtml);
        $beautifyHtml = "<p>" . nl2br($beautifyHtml) . "</p>";

        return $beautifyHtml;
    }

    private function formatPhone($dirtyNumber)
    {
        $beautifyNumber = trim($dirtyNumber);

        // fix numbers like +7 (3912) 200-00-00
        preg_match("/\+\s?7\s?\(3912\)\s?(?<number>\d{3}\-\d{2}\-\d{2})/", $beautifyNumber, $matches);
        if (isset($matches['number'])) {
            $beautifyNumber = "+7 (391) " . $matches['number'];
            return $beautifyNumber;
        }

        // fix numbers like +7 (3912) 00-00-00
        preg_match("/\+\s?7\s?\(3912\)\s?(?<number>\d{2}\-\d{2}\-\d{2})/", $beautifyNumber, $matches);
        if (isset($matches['number'])) {
            $beautifyNumber = "+7 (391) 2" . $matches['number'];
            return $beautifyNumber;
        }

        // fix numbers like +7 (391) 00-00-00
        preg_match("/\+\s?7\s?\(391\)\s?(?<number>\d{2}\-\d{2}\-\d{2})/", $beautifyNumber, $matches);
        if (isset($matches['number'])) {
            $beautifyNumber = "+7 (391) 2" . $matches['number'];
            return $beautifyNumber;
        }


        // number must count 11 numbers
        preg_match_all("/\d{1}/", $beautifyNumber, $matches);
        if (!(isset($matches[0]) && count($matches[0]) == 11)) {
            return null;
        }

        preg_match("/\+\s?7\s?\((?<code>\d{3})\)\s?(?<number>[\d\s\-]+)\s?/", $beautifyNumber, $matches);
        if (isset($matches['code']) && isset($matches['number'])) {
            $beautifyNumber = "+7 (" . $matches['code'] . ") " . $matches['number'];
            return $beautifyNumber;
        }


        return null;
    }
}
