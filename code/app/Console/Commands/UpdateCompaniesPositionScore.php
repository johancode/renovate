<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use DB;
use App;

class UpdateCompaniesPositionScore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:update-companies-position-score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update «position_score» for companies.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('queue')->setDefaultDriver('sync');

        $companies = Company::where('resaved', false);

        foreach ($companies->cursor() as $company) {
            $company->position_score = 0;
            $company->resaved = true;
            $company->save();

            echo "ID: {$company->id}, NAME: «{$company->name}» was updated\n";
        }

        echo "------\n";
        echo "DONE\n";
    }
}
