<?php

namespace App\Console\Commands;

use Elasticsearch\Client;
use Illuminate\Console\Command;

class ElasticSearchUpdateIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:update-index {mapName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex all records of mysql table';


    /**
     * Elastic Search client
     *
     * @var \Elasticsearch\Client
     */
    private $ESClient;

    /**
     * Create a new command instance.
     *
     * @param Client $client
     * @return void
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->ESClient = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mapName = '\\App\\ElasticSearch\\Mappings\\' . $this->argument('mapName');
        $map = new $mapName();
        $model = $map->getModel();


        $this->info("Indexing table «{$map->getSearchIndex()}». Might take a while...");

        foreach ($model->cursor() as $item) {
            $es_body = $map->toSearchArray($item);
            $this->ESClient->index([
                'index' => $map->getSearchIndex(),
                'type' => $map->getSearchType(),
                'id' => $es_body['id'],
                'body' => $es_body,
            ]);

            $this->output->write('.');
        }

        $this->info("\nDone!");
    }
}
