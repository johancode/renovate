<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;
use DB;
use Carbon\Carbon;


class GrabFlampComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:grab-flamp-comments {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab flamp comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private $maxSteps = 50;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('reset')) {
            DB::table("companies")->update(['resaved' => false]);

            echo("reset field 'resaved' done\n");
            return true;
        }


        echo("start grabbing\n");


        $hasCompanies = Company::where("resaved", false)->first();


        if (!$hasCompanies) {
            echo "all companies resaved\n";
            return;
        }


        while ($this->maxSteps) {
            sleep(rand(1, 3));

            $comments = [];
            $company = Company::where("resaved", false)
//            ->whereId(3871)
                ->orderBy('name')
                ->first();


            $companyName = urlencode($company->name);
            $commentsHeaders = $this->getDataViaCurl("https://krasnoyarsk.flamp.ru/search/{$companyName}", true);


            if (strpos($commentsHeaders, "HTTP/1.1 302 Found") !== false) {
                $commentsHtml = $this->getDataViaCurl("https://krasnoyarsk.flamp.ru/search/{$companyName}");
                $crawler = new Crawler($commentsHtml);

                $crawler->filter(".ugc-list__item")->each(function (Crawler $node, $i) use (&$comments) {
                    $rate = (int)trim($node->filter("meta[itemprop=ratingValue]")->attr("content"));

                    if ($rate < 4) {
                        return;
                    }


                    $dateCreatedText = trim($node->filter("meta[itemprop=dateCreated]")->attr("content"));
                    $dateCreated = new Carbon($dateCreatedText);


                    $comments[] = [
                        "author_name" => trim($node->filter(".author__string-part")->first()->text()),
                        "text" => trim($node->filter(".ugc-item__text--full")->first()->text()),
                        "source" => 2,
                        "created_at" => $dateCreated
                    ];
                });
            }

            $thread = $company->getThread();

            foreach ($comments as $comment) {
                $thread->comments()->create($comment);
            }

            $company->resaved = true;
            $company->save();

            echo "grabed: " . $company->name . (count($comments) ? (" (added " . count($comments) . " comments)") : "") . "\n";

            $this->maxSteps--;
        }
    }


    private function getDataViaCurl($uri, $headers = false)
    {
        $ch = curl_init();

        if ($headers) {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_VERBOSE, true);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
        } else {
//            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }


        curl_setopt($ch, CURLOPT_URL, $uri);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($ch);

        curl_close($ch);


        return $response;
    }
}
