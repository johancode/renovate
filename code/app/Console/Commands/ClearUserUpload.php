<?php

namespace App\Console\Commands;

use App\Models\UserUploadImage;
use Illuminate\Console\Command;

class ClearUserUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploads:clear-user-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear user uploads temp files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new \Carbon\Carbon('1 month ago');

        $images = UserUploadImage::where("created_at", "<", $date)
            ->take(100)
            ->get();

        foreach ($images as $image) {
            $image->delete();
        }
    }
}
