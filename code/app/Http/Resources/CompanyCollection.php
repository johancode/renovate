<?php

namespace App\Http\Resources;

use App\Repositories\CompanyCategory\CompanyCategoriesRepository;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App;

class CompanyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $result = ['data' => $this->collection];

        if ($request->withFilters) {
            $categoriesTree = App::make(CompanyCategoriesRepository::class)
                ->getTree()
                ->values()
                ->toArray();

            $result['filters'] = [
                'categoriesTree' => $categoriesTree
            ];
        }

        return $result;
    }
}
