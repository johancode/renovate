<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Company extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];

        $response['id'] = $this->id;
        $response['name'] = $this->name;
        $response['url'] = $this->url;
        $response['phone'] = $this->phone;
        $response['info'] = $this->info;
        $response['link'] = $this->link;
        $response['specialization'] = $this->specialization;

        $response['images_count'] = $this->images_count;
        $response['comments_count'] = $this->comments_count;
        $response['services_count'] = $this->services_count;

//        $response['index'] = $this->position_index . " [" . $this->position_score . "]";

        return $response;
    }
}
