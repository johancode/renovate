<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\CompanyMasterCategory;
use App\Models\CompanyService;
use App\Repositories\CompanyCategory\CompanyCategoriesRepository;
use Illuminate\Http\Request;
use Auth;
use App;
use DB;

class CompaniesController extends Controller
{
    private $categoriesRepo;

    function __construct(CompanyCategoriesRepository $categories)
    {
        $this->categoriesRepo = $categories;
    }

    function showCompaniesList()
    {
        $pageTitle = "Строительные и ремонтные услуги";
        $pageSubTitle = "Компании в Красноярске";

        return view('companies.pages.companies-list')->with(compact('pageTitle', 'pageSubTitle'));
    }


    function showCategories(Request $request)
    {
        $pageTitle = "Строительство и ремонт в Красноярске";
        $filterOptions = [
            "services" => [
                "active" => false,
                "link" => route('companies.categories') . "?type=services",
            ],
            "shops" => [
                "active" => false,
                "link" => route('companies.categories') . "?type=shops",
            ],
        ];


        switch ($request->type) {
            case "services":
                $this->categoriesRepo->setTypeFilter("is_service");

                $filterOptions['services']['active'] = true;
                $filterOptions['services']['link'] = route('companies.categories');
                $pageTitle = "Строительные и ремонтные услуги";
                break;

            case "shops":
                $this->categoriesRepo->setTypeFilter("is_shop");

                $filterOptions['shops']['active'] = true;
                $filterOptions['shops']['link'] = route('companies.categories');
                $pageTitle = "Строительные и отделочные материалы";
                break;
        }

        $categoriesList = $this->categoriesRepo->getTree();


        $masterCategories = CompanyMasterCategory::orderBy('lft')
            ->whereIn("id", $categoriesList->keys())
            ->pluck('name')
            ->toArray();

        \SEOMeta::setDescription("Стрительство и ремонт в Красноярске: " . implode(", ", $masterCategories));


        return view('companies.pages.categories')
            ->with(compact(
                'categoriesList',
                'filterOptions',
                'pageTitle'
            ));
    }


    function showCompaniesByCategory($companyUrl)
    {
        $category = CompanyCategory::active()
            ->where('url', $companyUrl)
            ->firstOrFail();

        $pageTitle = $category->name;
        $pageSubTitle = $category->subtitle;

        \SEOMeta::setTitle($category->name . " в Красноярске");
        \OpenGraph::setTitle($category->name . " в Красноярске");


        if ($category->meta_description) {
            \SEOMeta::setDescription($category->meta_description);
        } else if ($category->subtitle) {
            \SEOMeta::setDescription($category->subtitle . ". " . $category->name . " в Красноярске");
        }

        if ($category->meta_keywords) {
            \SEOMeta::setKeywords($category->meta_keywords);
        }


        return view('companies.pages.companies-list')
            ->with(compact('category', 'pageTitle', 'pageSubTitle'));
    }


    function showServices()
    {
        $services = CompanyService::active()
//            ->withCount('companies')
//            ->orderBy('companies_count')
            ->orderBy('name')
            ->get();


        \SEOMeta::setTitle("Строительные и ремонтные услуги в Красноярске");
        \OpenGraph::setTitle("Строительные и ремонтные услуги в Красноярске");


        return view('companies.pages.services')
            ->with(compact('services'));
    }

    function showCompaniesByService($serviceUrl)
    {
        $service = CompanyService::active()
            ->where('url', $serviceUrl)
            ->firstOrFail();

        $companies = Company::active()
            ->whereHas('services', function ($query) use ($service) {
                $query->where('service_id', '=', $service->id);
            })
            ->withServicePrice($service->id)
            ->orderBy('position_index')
            ->paginate(10);


        \SEOMeta::setTitle($service->name . " в Красноярске");
        \OpenGraph::setTitle($service->name . " в Красноярске");


        return view('companies.pages.service')->with(compact(
            'service',
            'companies'
        ));
    }


    function showCompany(Request $request, $companyUrl)
    {
        $company = Company::active()
            ->where('url', $companyUrl)
            ->firstOrFail();

        $services = $company
            ->services()
            ->active()
            ->withCount('companies')
            ->orderBy('name')
            ->get()
            ->mapWithKeys(function ($item, $key) {
                return [$key => (object)[
                    'name' => $item->name,
                    'unit' => $item->unit_type,
                    'price' => $item->pivot->price,
                    'link' => $item->link,
                    'companies_count' => $item->companies_count
                ]];
            });

        $categories = $company
            ->categories()
            ->active()
            ->orderBy('name')
            ->get();


        $contacts = $company->getContacts();


        $thread = $company->getThread();

        $comments = $thread
            ->comments()
            ->orderBy("created_at", "desc")
            ->take(5)
            ->get();


        $categoriesDesc = $categories->implode('name', ', ');
        $shortDesc = str_limit(strip_tags($company->info), 140);
        $companyInfo = $categoriesDesc ? $categoriesDesc : $shortDesc;
        $companyDesc = $company->name . ($companyInfo ? (": " . mb_strtolower($companyInfo)) : "") . " (Красноярск)";


        \SEOMeta::setTitle($company->name . " (Красноярск): контакты, прайс-лист, отзывы", false);
        \SEOMeta::setDescription($companyDesc);

        \OpenGraph::setTitle($company->name);
        \OpenGraph::setUrl($company->link);
        \OpenGraph::setDescription($companyDesc);

        if ($company->images) {
            $num = 1;
            foreach ($company->images as $img) {
                if ($num > 5) {
                    break;
                }
                \OpenGraph::addImage($img->modal);
                $num++;
            }
        }

        $showShareBox = false;
        $isOwner = Auth::user() ? (Auth::user()->id == @$company->user->id) : false;
        if (($request->ac == "ref") && $isOwner) {
            $showShareBox = true;
        }


        return view('companies.pages.company')->with(compact(
            'company',
            'services',
            'comments',
            'thread',
            'categories',
            'contacts',
            'showShareBox'
        ));
    }
}
