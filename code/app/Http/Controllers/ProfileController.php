<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\{
    CompanyCategory, CompanyMasterCategory
};
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    function showDashboard()
    {
        return redirect("/profile/companies");
        return view('profile.pages.dashboard');
    }

    function showUserForm()
    {
        return view('profile.pages.user-form');
    }

    function showCompaniesList()
    {
        $companies = $this
            ->user
            ->companies()
            ->orderBy("created_at", "desc")
            ->paginate(10);

        return view('profile.pages.companies-list')
            ->with(compact('companies'));
    }

    function showUserPasswordForm()
    {
        return view('profile.pages.user-password-form');
    }

    function showCompanyCreateForm()
    {
        return view('profile.pages.company-create');
    }

    function showCompanyEditForm(Request $request, $companyId)
    {
        $company = Company::findOrFail($companyId);
        return view('profile.pages.company-edit')->with(compact("company"));
    }

    function showUserNotifyForm()
    {
        return view('profile.pages.user-notify-form');
    }
}
