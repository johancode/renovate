<?php

namespace App\Http\Controllers\Admin;


use Config;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\Admin\CompanyServiceRequest as StoreRequest;
use App\Http\Requests\Admin\CompanyServiceRequest as UpdateRequest;

class CompanyServiceCrudController extends CrudController
{
    public function setUp()
    {
        $this->crud->setModel("App\Models\CompanyService");
        $this->crud->setRoute(Config::get("backpack.base.route_prefix") . "/service");
        $this->crud->setEntityNameStrings('service', 'services');

        if (!$this->request->order) {
            $this->crud->orderBy('name');
        }

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'name',
                    'label' => 'Название',
                ],
                [
                    'name' => 'url',
                    'label' => 'URL',
                ],
                [
                    'name' => 'active',
                    'label' => 'Отображать',
                    'type' => 'check'
                ]
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'name',
                    'label' => 'Название',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'unit_type',
                    'label' => 'Единица измерения',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'url',
                    'label' => 'url',
                    'hint' => 'Если не указано - формируется на основе названия',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'active',
                    'label' => 'Активен, отображать на сайте',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ],
                [
                    'label' => 'Категории',
                    'type' => 'checklist',
                    'name' => 'categories',
                    'entity' => 'categories',
                    'attribute' => 'name',
                    'model' => \App\Models\CompanyCategory::class,
                    'pivot' => true,
                    'tab' => 'Категории'
                ]
            ]);
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud();
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud();
        return $redirect_location;
    }
}
