<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use Config;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\Admin\CompanyRequest as StoreRequest;
use App\Http\Requests\Admin\CompanyRequest as UpdateRequest;

class CompanyCrudController extends CrudController
{
    public function setup()
    {
        $contactTypes = collect(Company::getContactTypesList())
            ->map(function ($value, $key) {
                return $value . ": " . $key;
            })
            ->implode('<br>');


        $this->crud->setModel(\App\Models\Company::class);
        $this->crud->setRoute(Config::get("backpack.base.route_prefix") . "/company");
        $this->crud->setEntityNameStrings('company', 'companies');

        if (!$this->request->order) {
            $this->crud->orderBy('name');
        }

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'name',
                    'label' => 'Название',
                ],
                [
                    'name' => 'phone',
                    'label' => 'Телефон',
                ],
                [
                    'name' => 'specialization',
                    'label' => 'Спец-ия',
                ],
                [
                    'name' => 'updated_at',
                    'type' => 'datetime',
                    'label' => 'Дата изменения',
                    'searchLogic' => false
                ],
                [
                    'name' => 'active',
                    'label' => '',
                    'type' => 'check'
                ]
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'name',
                    'label' => 'Название',
                    'tab' => 'Основное',
                ],
                [
                    'label' => "Логотип",
                    'name' => "logo",
                    'type' => 'images_with_preview',
                    'tab' => 'Изображения',
                ],
                [
                    'label' => "Изображения",
                    'name' => "images",
                    'type' => 'images_with_preview',
                    'multiple' => true,
                    'tab' => 'Изображения',
                ],
                [
                    'name' => 'specialization',
                    'label' => 'Специализация',
                    'type' => 'textarea',
                    'tab' => 'Информация',
                ],
                [
                    'name' => 'info',
                    'label' => 'Описание',
                    'type' => 'texteditor',
                    'tab' => 'Информация',
                ],
                [
                    'name' => 'url',
                    'label' => 'URL',
                    'hint' => 'Если не указано - формируется на основе названия',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'services_list',
                    'label' => 'Услуги',
                    'type' => 'company_services',
                    'tab' => 'Услуги',
                ],
                [
                    'label' => 'Категории',
                    'type' => 'checklist',
                    'name' => 'categories',
                    'entity' => 'categories',
                    'attribute' => 'name',
                    'model' => \App\Models\CompanyCategory::class,
                    'pivot' => true,
                    'tab' => 'Категории'
                ],
                [
                    'name' => 'phone',
                    'label' => 'Телефон основной',
                    'tab' => 'Контакты',
                ],
                [
                    'name' => 'contacts',
                    'label' => 'Контакты',
                    'type' => 'table',
                    'entity_singular' => 'contact',
                    'columns' => [
                        'type' => 'Тип контакта',
                        'value' => 'Значание',
                    ],
                    'hint' => 'Типы контактов:<br>' . $contactTypes,
                    'max' => 10,
                    'min' => 0,
                    'tab' => 'Контакты',
                ],
                [
                    'name' => 'addresses',
                    'label' => 'Адреса',
                    'type' => 'table',
                    'hint' => '<a href="https://www.latlong.net" target="_blank">find coordinates</a>',
                    'entity_singular' => 'point',
                    'columns' => [
                        'address' => 'Адрес',
                        'coordinates' => 'Координаты',
                        'info' => 'Доп. информация'
                    ],
                    'max' => 10,
                    'min' => 0,
                    'tab' => 'Контакты',
                ],
                [
                    'name' => 'user_rate',
                    'label' => 'Рейтинг',
                    'type' => 'radio',
                    'options' => [
                        0 => "0",
                        1 => "1",
                        2 => "2",
                        3 => "3",
                        4 => "4",
                        5 => "5",
                    ],
                    'inline' => true,
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'source',
                    'label' => 'Источник',
                    'type' => 'select_from_array',
                    'options' => Company::getSourcesList(),
                    'allows_null' => false,
                    'tab' => 'Основное',
                ],
                [
                    'label' => "Пользователь",
                    'type' => "select2_from_ajax",
                    'name' => 'user_id',
                    'entity' => 'user',
                    'attribute' => "name",
                    'model' => \App\User::class,
                    'data_source' => url(config('backpack.base.route_prefix', 'admin') . "/api/user"),
                    'placeholder' => "Select a user",
                    'minimum_input_length' => 2,
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'active',
                    'label' => 'Отображать на сайте',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'moderated',
                    'label' => 'Модерация пройдена',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'resaved',
                    'label' => 'resaved',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ]
            ]);


        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'active',
                'label' => 'Отображать'
            ],
            [
                1 => "Да",
                0 => "Нет"
            ],
            function ($value) {
                $this->crud->addClause('where', 'active', $value);
            });

        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'moderated',
                'label' => 'Модерация'
            ],
            [
                1 => "Пройдена",
                0 => "Не пройдена"
            ],
            function ($value) {
                $this->crud->addClause('where', 'moderated', $value);
            });

        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'resaved',
                'label' => 'Resaved'
            ],
            [
                1 => "Yes",
                0 => "No"
            ],
            function ($value) {
                $this->crud->addClause('where', 'resaved', $value);
            });

        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'type',
                'label' => 'Источник'
            ],
            Company::getSourcesList(),
            function ($value) {
                $this->crud->addClause('where', 'source', $value);
            });
    }

    public function edit($id)
    {
        $thread = $this->crud->getEntry($id)->getThread();


        $links = "";
        $commentsLink = url(config('backpack.base.route_prefix', 'admin') . '/comment?thread=') . $thread->id;
        $links .= "<a href='{$commentsLink}' target='_blank'>Комментарии пользователей</a>";

        $companyLink = $this->crud->getEntry($id)->link;
        $links .= "<br><a href='{$companyLink}' target='_blank'>Страница компании</a>";

        $this
            ->crud
            ->addField(
                [
                    'name' => 'comments_link',
                    'type' => 'custom_html',
                    'value' => $links,
                    'tab' => 'Дополнительно',
                ]
            );

        return parent::edit($id);
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }
}
