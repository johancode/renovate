<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\TextBlockRequest as StoreRequest;
use App\Http\Requests\Admin\TextBlockRequest as UpdateRequest;
use Auth;

class TextBlockCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(\App\Models\TextBlock::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/text-block');
        $this->crud->setEntityNameStrings('Блок', 'Блоки');

        $this->crud->orderBy('key');

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'key',
                    'label' => 'Название',
                ],
                [
                    'name' => 'value',
                    'label' => 'Значение',
                ]
            ]);


        $this
            ->crud
            ->addField([
                'name' => 'key',
                'label' => 'key',
                'type' => 'custom_html',
            ]);


        if (Auth::getUser()->hasRole('admin')) {
            $this
                ->crud
                ->addField([
                    'name' => 'is_html',
                    'label' => 'is_html',
                    'type' => 'checkbox'
                ]);
        } else {
            $this->crud->removeButton('delete');
        }
    }


    public function edit($id)
    {
        if ($this->crud->getEntry($id)->is_html) {
            $this
                ->crud
                ->addField([
                    'name' => 'value',
                    'type' => 'texteditor',
                    'label' => 'Значение',
                ]);
        } else {
            $this
                ->crud
                ->addField([
                    'name' => 'value',
                    'label' => 'Значение',
                ]);
        }

        return parent::edit($id);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
