<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\PageRequest as StoreRequest;
use App\Http\Requests\Admin\PageRequest as UpdateRequest;

class PageCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(\App\Models\Page::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('страницу', 'страниц');

        $this->crud->orderBy('title');

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'title',
                    'label' => 'Заголовок',
                ],
                [
                    'name' => 'url',
                    'label' => 'url',
                ],
                [
                    'name' => 'active',
                    'label' => '',
                    'type' => 'check'
                ]
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'title',
                    'label' => 'Заголовок',
                ],
                [
                    'name' => 'url',
                    'label' => 'url',
                    'hint' => 'Если не указано - формируется на основе названия',
                ],
                [
                    'name' => 'active',
                    'label' => 'Отображать на сайте',
                    'type' => 'checkbox',
                ],
                [
                    'name' => 'content',
                    'label' => 'Текст',
                    'type' => 'texteditor',
                ]
            ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
