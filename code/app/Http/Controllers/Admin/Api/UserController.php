<?php

namespace App\Http\Controllers\Admin\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class UserController extends BaseController
{
    public function index(Request $request)
    {
        $searchTerm = $request->q;
        $page = $request->page;

        $user = User::orderBy("name");


        if ($searchTerm) {
            $user = $user->where('name', 'LIKE', '%' . $searchTerm . '%');
        }

        if ($request->ids) {
            $user = $user->whereIn('id', $request->ids);
        }


        $limit = $request->limit;
        if (!$limit) {
            $limit = 20;
        }

        $user = $user->paginate($limit);


        return $user;
    }

    public function show($id)
    {
        return User::find($id);
    }
}
