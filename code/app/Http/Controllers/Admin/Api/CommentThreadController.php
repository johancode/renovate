<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\CommentThread;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CommentThreadController extends BaseController
{
    public function index(Request $request)
    {
        $searchTerm = $request->q;
        $page = $request->page;

        $threads = CommentThread::orderBy("title");


        if ($searchTerm) {
            $threads = $threads->where('title', 'LIKE', '%' . $searchTerm . '%');
        }

        if ($request->ids) {
            $threads = $threads->whereIn('id', $request->ids);
        }


        $limit = $request->limit;
        if (!$limit) {
            $limit = 20;
        }

        $threads = $threads->paginate($limit);


        return $threads;
    }

    public function show($id)
    {
        return CommentThread::find($id);
    }
}
