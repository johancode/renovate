<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\CompanyService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class ServiceController extends BaseController
{
    public function index(Request $request)
    {
        $searchTerm = $request->q;
        $page = $request->page;

        $services = CompanyService::orderBy("name");


        if ($searchTerm) {
            $services = $services->where('name', 'LIKE', '%' . $searchTerm . '%');
        }

        if ($request->ids) {
            $services = $services->whereIn('id', $request->ids);
        }


        $limit = $request->limit;
        if (!$limit) {
            $limit = 20;
        }

        $services = $services->paginate($limit);


        return $services;
    }

    public function parse(Request $request)
    {
        $response = [];


        $servicesAttempts = $request->services ? json_decode($request->services, true) : [];
        foreach ($servicesAttempts as $serviceAttempt) {
            if ($serviceAttempt['name'] && $serviceAttempt['price']) {
                $name = str_replace("\n", "", $serviceAttempt['name']);
                $nameFirstWord = @explode(" ", $name)[0];

                $serviceResult = [
                    "originalName" => $name,
                    "price" => $serviceAttempt['price'],
                ];


                $serviceResult["variants"] = CompanyService::select('name', 'id', 'unit_type')
                    ->where('name', $name)
//                    ->orWhere('name', 'like', "%{$nameFirstWord}%")
                    ->take(5)
                    ->get()
                    ->toArray();

                if ($serviceResult["variants"]) {
                    $response[] = $serviceResult;
                }
            }
        }


        return $response;
    }
}
