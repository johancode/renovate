<?php

namespace App\Http\Controllers\Admin;

use Config;
use App\Models\GrabberRecord;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\GrabberRecordRequest as StoreRequest;
use App\Http\Requests\Admin\GrabberRecordRequest as UpdateRequest;

class GrabberRecordCrudController extends CrudController
{

    public function setUp()
    {
        $this->crud->setModel(\App\Models\GrabberRecord::class);
        $this->crud->setRoute(Config::get("backpack.base.route_prefix") . "/grabber-record");
        $this->crud->setEntityNameStrings('record', 'records');

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'title',
                    'label' => 'Название',
                ],
                [
                    'name' => 'source',
                    'label' => 'Источник',
                ],
                [
                    'name' => 'type',
                    'label' => 'Тип данных',
                ],
//            [
//                'name' => 'uri',
//                'label' => 'uri',
//            ],
//            [
//                'name' => 'updated_at',
//                'label' => 'Дата изменения',
//            ],
                [
                    'name' => 'status',
                    'label' => 'Статус',
                ]
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'title',
                    'label' => 'Название компании',
                ],
                [
                    'name' => 'uri',
                    'label' => 'Ссылка',
                ],
                [
                    'name' => 'source',
                    'label' => 'Источинк',
                ],
                [
                    'name' => 'type',
                    'label' => 'Тип данных',
                ],
                [
                    'name' => 'status',
                    'label' => 'Статус',
                ],
                [
                    'name' => 'hr',
                    'type' => 'custom_html',
                    'value' => '<hr>',
                ],
                [
                    'name' => 'content',
                    'label' => 'Данные',
                    'type' => 'dump'
                ]
            ]);


        $all_sources = GrabberRecord::select('source')
            ->groupBy('source')
            ->get()
            ->pluck('source')
            ->mapWithKeys(function ($item) {
                return [$item => $item];
            })
            ->toArray();

        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'source',
                'label' => 'Источник'
            ],
            $all_sources,
            function ($value) {
                $this->crud->addClause('where', 'source', $value);
            });


        $all_statuses = GrabberRecord::select('status')
            ->groupBy('status')
            ->get()
            ->pluck('status')
            ->mapWithKeys(function ($item) {
                return [$item => $item];
            })
            ->toArray();

        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'status',
                'label' => 'Статус'
            ],
            $all_statuses,
            function ($value) {
                $this->crud->addClause('where', 'status', $value);
            });


        $all_types = GrabberRecord::select('type')
            ->groupBy('type')
            ->get()
            ->pluck('type')
            ->mapWithKeys(function ($item) {
                return [$item => $item];
            })
            ->toArray();

        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'type',
                'label' => 'Тип данных'
            ],
            $all_types,
            function ($value) {
                $this->crud->addClause('where', 'type', $value);
            });
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
