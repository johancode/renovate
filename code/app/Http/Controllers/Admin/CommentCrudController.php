<?php

namespace App\Http\Controllers\Admin;

use App\Models\CommentThread;
use Config;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\CommentRequest as StoreRequest;
use App\Http\Requests\CommentRequest as UpdateRequest;

class CommentCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(\App\Models\Comment::class);
        $this->crud->setRoute(Config::get("backpack.base.route_prefix") . "/comment");
        $this->crud->setEntityNameStrings('comment', 'comments');

        if (!$this->request->order) {
            $this->crud->orderBy('created_at');
        }

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'created_at',
                    'label' => 'Добавлено',
                    'type' => 'datetime'
                ],
                [
                    'label' => "Тема",
                    'type' => "select",
                    'name' => 'thread_id',
                    'entity' => 'thread',
                    'attribute' => "title",
                    'model' => \App\Models\CommentThread::class,
                ],
                [
                    'name' => 'text',
                    'label' => 'Текст',
                ],
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'author_name',
                    'label' => 'Автор',
                    'hint' => 'Отображается если не выбран пользователь'
                ],
                [
                    'label' => "Пользователь",
                    'type' => "select2_from_ajax",
                    'name' => 'user_id',
                    'entity' => 'user',
                    'attribute' => "name",
                    'model' => \App\User::class,
                    'data_source' => url(config('backpack.base.route_prefix', 'admin') . "/api/user"),
                    'placeholder' => "Select a user",
                    'minimum_input_length' => 2,
                ],
                [
                    'name' => 'text',
                    'label' => 'Текст',
                    'type' => 'textarea',
                ],
                [
                    'name' => 'source',
                    'label' => 'Источник',
                    'type' => 'select_from_array',
                    'options' => \App\Models\Comment::getSourcesList(),
                    'allows_null' => false,
                ],
            ]);


        // add threads filter
        if ($this->request->thread) {
            $thread = CommentThread::whereId($this->request->thread)->firstorFail();
            $this->crud->addClause('where', 'thread_id', $thread->id);

            $this->crud->entity_name_plural .= " ({$thread->title})";

            $this->crud->addButtonFromView("top", "add_comment", "add_comment", "end");
        }
    }


    public function edit($id)
    {
        $this
            ->crud
            ->addField([
                'label' => "Тема",
                'type' => "select2_from_ajax",
                'name' => 'thread_id',
                'entity' => 'thread',
                'attribute' => "title",
                'model' => \App\Models\CommentThread::class,
                'data_source' => url(config('backpack.base.route_prefix', 'admin') . "/api/c-thread"),
                'placeholder' => "Select a thread",
                'minimum_input_length' => 2,
            ]);

        return parent::edit($id);
    }

    public function create()
    {
        if ($this->request->thread) {
            $thread = CommentThread::whereId($this->request->thread)->firstorFail();

            $this
                ->crud
                ->addField([
                    'name' => 'thread_id',
                    'type' => 'hidden',
                    'value' => $thread->id
                ]);

            $this->crud->entity_name .= " «{$thread->title}»";
        } else {
            $this
                ->crud
                ->addField([
                    'label' => "Тема",
                    'type' => "select2_from_ajax",
                    'name' => 'thread_id',
                    'entity' => 'thread',
                    'attribute' => "title",
                    'model' => \App\Models\CommentThread::class,
                    'data_source' => url(config('backpack.base.route_prefix', 'admin') . "/api/c-thread"),
                    'placeholder' => "Select a thread",
                    'minimum_input_length' => 2,
                ]);
        }

        return parent::create();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
