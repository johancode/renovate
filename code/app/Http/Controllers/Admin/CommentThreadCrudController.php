<?php

namespace App\Http\Controllers\Admin;

use Config;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\CommentThemeRequest as StoreRequest;
use App\Http\Requests\CommentThemeRequest as UpdateRequest;

class CommentThreadCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(\App\Models\CommentThread::class);
        $this->crud->setRoute(Config::get("backpack.base.route_prefix") . "/c-thread");
        $this->crud->setEntityNameStrings('theme', 'themes');

        if (!$this->request->order) {
            $this->crud->orderBy('created_at');
        }

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'title',
                    'label' => 'Название',
                ],
                [
                    'name' => 'created_at',
                    'label' => 'Добавлено',
                    'type' => 'datetime'
                ]
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'title',
                    'label' => 'Название',
                ],
            ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
