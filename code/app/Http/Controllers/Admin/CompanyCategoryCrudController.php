<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompanyCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\CompanyCategoryRequest as StoreRequest;
use App\Http\Requests\Admin\CompanyCategoryRequest as UpdateRequest;

class CompanyCategoryCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(\App\Models\CompanyCategory::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/company-category');
        $this->crud->setEntityNameStrings('category', 'categories');

        if (!$this->request->order) {
            $this->crud->orderBy('full_name');
        }

        $this->crud->setDefaultPageLength(10);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'full_name',
                    'label' => 'Раздел + категория',
                ],
                [
                    'name' => 'name',
                    'label' => 'Название',
                ],
                [
                    'name' => 'url',
                    'label' => 'URL',
                ],
                [
                    'name' => 'active',
                    'label' => 'Отображать',
                    'type' => 'check'
                ]
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'name',
                    'label' => 'Название',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'url',
                    'label' => 'url',
                    'hint' => 'Если не указано - формируется на основе названия',
                    'tab' => 'Основное',
                ],
                [
                    'label' => "Раздел",
                    'type' => 'select',
                    'name' => 'master_category_id',
                    'entity' => 'masterCategory',
                    'attribute' => 'name',
                    'model' => \App\Models\CompanyMasterCategory::class,
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'subtitle',
                    'label' => 'Доп заголовок',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'seo_title',
                    'label' => 'SEO заголовок',
                    'tab' => 'SEO',
                ],
                [
                    'name' => 'seo_text',
                    'label' => 'SEO описание',
                    'type' => 'texteditor',
                    'tab' => 'SEO',
                ],
                [
                    'name' => 'active',
                    'label' => 'Активен, отображать на сайте',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'meta_description',
                    'label' => 'META: description',
                    'type' => 'textarea',
                    'hint' => 'Если не указано - формируется по шаблону «Описание» + «Название» + в Красноярске',
                    'tab' => 'SEO',
                ],
                [
                    'name' => 'meta_keywords',
                    'label' => 'META: keywords',
                    'tab' => 'SEO',
                ],
                [
                    'name' => 'is_shop',
                    'label' => 'Тип компаний - продажа',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ],
                [
                    'name' => 'is_service',
                    'label' => 'Тип компаний - услуги',
                    'type' => 'checkbox',
                    'tab' => 'Основное',
                ],
            ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
