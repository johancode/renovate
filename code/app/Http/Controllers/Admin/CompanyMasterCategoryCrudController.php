<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MasterCategoryRequest as StoreRequest;
use App\Http\Requests\MasterCategoryRequest as UpdateRequest;

class CompanyMasterCategoryCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(\App\Models\CompanyMasterCategory::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/company-master-category');
        $this->crud->setEntityNameStrings('category', 'categories');

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');

        if (!$this->request->order) {
            $this->crud->orderBy('lft');
        }

        $this->crud->setDefaultPageLength(50);


        $this
            ->crud
            ->addColumns([
                [
                    'name' => 'name',
                    'label' => 'Название',
                ],
                [
                    'name' => 'url',
                    'label' => 'URL',
                ],
            ]);


        $this
            ->crud
            ->addFields([
                [
                    'name' => 'name',
                    'label' => 'Название'
                ],
                [
                    'name' => 'icon_label',
                    'label' => 'icon_label'
                ],
                [
                    'name' => 'url',
                    'label' => 'url',
                    'hint' => 'Если не указано - формируется на основе названия',
                ],
            ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
