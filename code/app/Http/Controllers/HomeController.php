<?php

namespace App\Http\Controllers;

use App\Mail\SimpleTest;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\CompanyService;
use App\User;
use Illuminate\Http\Request;
use Artisan;
use Mail;
use App;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        DB::table("comments")->delete();
//
//        Artisan::call('website:grab-flamp-comments');


//        die();

        return redirect("/");

//        $this->setAppendCategoryByDesc();
//        $this->fixCompanies();
//        $this->resetResaved();

//        $this->moveToOneCategory();

//        $this->appendCategory();

//        dd("--- done ---");

//        return view('home');
    }


    private function moveToOneCategory()
    {
        $companies = $companies = Company::whereHas('categories', function ($query) {
            $query->whereIn('url', [
                'makety-i-prototipirovanie',
            ]);
        })->get();

        $newCategory = CompanyCategory::where('url', 'stroitelnoe-proektirovanie')->first();

        foreach ($companies as $company) {
            $company->categories()->syncWithoutDetaching($newCategory->id);
        }
    }

    private function appendCategory()
    {
        $categoriesIds = CompanyCategory::whereIn('url', [
            'napolnye-pokrytiya-ukladka'
        ])
            ->get()
            ->pluck('id')
            ->toArray();


        $companies = Company::whereHas('services', function ($query) {
            $query->whereIn('url', [
                'ukladka-linoleuma-kovrolina',
                'ukladka-parketnoy-doski',
                'ukladka-parketa',
            ]);
        })->get();

        foreach ($companies as $company) {
            $company->categories()->syncWithoutDetaching($categoriesIds);
        }
    }

    private function fixCompanies()
    {

        // check all phone numbers

//        $companies = Company::active()
//            ->where('resaved', false)
//            ->inRandomOrder()
//            ->limit(1000)
//            ->get();
//
//        foreach ($companies as $company) {
//            $phone =$company->phone ;
//            $phone = str_replace([" ", "(", ")", "-"], "", $phone);
//
//            $company->phone = $phone;
//
//            $company->resaved = true;
//            $company->save();
//        }
//
//
//        dd("done");
    }

    private function resetResaved()
    {
        DB::table('companies')->update(['resaved' => 0]);
    }

    private function setAppendCategoryByDesc()
    {
        $categoriesIds = CompanyCategory::whereIn('url', [
            'potolki-montazh'
        ])
            ->get()
            ->pluck('id')
            ->toArray();

        // НЕ ДОРОГО
        // ВСЕ БЫСТРО КАЧЕСТВЕННО

        $companies = Company::where([
            ['moderated', false],
            ['active', 1],
            ['source', 2],
//            ['info', '<>', '<p></p>'],
//            ['images', null],
//            ['contacts', '[]'],
            ['info', 'like', '%отделк%']
        ])
            ->doesntHave('categories')
//            ->limit(100)
            ->get();

//        dd($companies->count());


        foreach ($companies as $company) {
            echo("<a href=\"$company->link\" target='_blank'>$company->link</a><br>");

//            $company->categories()->syncWithoutDetaching  ($categoriesIds);
//            $company->specialization = "печник";
//            $company->moderated = true;
//            $company->save();
        }
    }
}
