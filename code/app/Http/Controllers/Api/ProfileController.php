<?php

namespace App\Http\Controllers\Api;

use App\Models\CompanyCategory;
use App\Models\CompanyMasterCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function getCategories(Request $request)
    {
        $categories = CompanyCategory::active()
            ->orderBy('name')
            ->where('active', true)
            ->get()
            ->mapToGroups(function ($item, $key) {
                $categoryData = $item->only(['id', 'name', 'full_name', 'is_service', 'is_shop']);
                $categoryData['filterValues'] = [];

                if ($categoryData['is_shop']) {
                    $categoryData['filterValues'] = ['is_shop'];
                }
                if ($categoryData['is_service']) {
                    $categoryData['filterValues'] = ['is_service'];
                }

                return [$item->masterCategory->id => $categoryData];
            });

        $categoriesList = CompanyMasterCategory::orderBy('lft')
            ->get()
            ->mapWithKeys(function ($item) use ($categories) {
                return [$item->id => (object)[
                    'name' => $item->name,
                    'id' => $item->id,
//                    'icon_label' => $item->icon_label,
//                    'companies_score' => $item->companies_score,
                    'categories' => $categories[$item->id] ? $categories[$item->id]->toArray() : []
                ]];
            })
            ->transform(function ($item) {
                $categoriesFilter = [];

                foreach ($item->categories as $category) {
                    $categoriesFilter = array_merge($categoriesFilter, $category['filterValues']);
                }

                $categoriesFilter = array_unique($categoriesFilter);
                $item->filterValues = $categoriesFilter;
                return $item;
            });


        header('Content-type: application/json');

        return json_encode(array_values($categoriesList->toArray()), 1);
    }
}
