<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CompanyRequest;
use App\Mail\CreateCompany;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Repositories\Company\CompaniesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Company as CompanyResource;
use App\Http\Resources\CompanyCollection as CompanyCollectionResource;
use Auth;
use Mail;
use App;

class CompanyController extends Controller
{
    /**
     * The companies repository
     *
     * @var CompaniesRepository
     */
    protected $companiesRepository;

    /**
     * The settings utility
     *
     * @var \App\Helpers\SettingsHelper
     */
    protected $settings;

    /**
     * CompanyController constructor.
     *
     * @param CompaniesRepository $companiesRepository
     */
    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->settings = App::make("SettingsHelper");
        $this->companiesRepository = $companiesRepository;
    }

    /**
     * Show filtered companies list
     *
     * @param Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        $companies = $this->companiesRepository;

        if ($request->search) {
            $searchQuery = htmlspecialchars($request->search);
            $companies = $companies->setSearchQuery($searchQuery);
        }

        $companies = $companies
            ->setFilters($request->all())
            ->setOrder($request->order)
            ->setCurrentPage($request->page)
            ->setPerPageAmount(15)
            ->get();

        return new CompanyCollectionResource($companies);
    }

    public function store(CompanyRequest $request)
    {
        $company = new Company();
        $company->user_id = Auth::user()->id;
        $company->active = true;

        $this->setCompanyData($company, $request->all());
        // TODO: second save to remember the relations
        $this->setCompanyData($company, $request->all());


        Mail::to($this->settings->get("emails: new company"))->queue(new CreateCompany($request->all()));

        return response()->json([
            "status" => "ok",
            "company" => $company->fresh(),
            "link" => $company->link,
        ]);
    }

    public function show($companyId)
    {
        $company = Company::findOrFail($companyId);

        $categories = $company
            ->categories()
            ->get()
            ->pluck("id")
            ->toArray();

        $company->categories = $categories;

        $services = $company
            ->services()
            ->orderby('name')
            ->get()
            ->mapWithKeys(function ($item, $key) {
                return [$key => (object)[
                    'id' => $item->id,
                    'name' => $item->name,
                    'unit' => $item->unit_type,
                    'price' => $item->pivot->price,
                ]];
            });

        $company->services = $services;

        $company->contacts = $company->contacts;

        return response()->json($company);
    }

    public function update(CompanyRequest $request, $companyId)
    {
        $company = Company::findOrFail($companyId);
        $this->setCompanyData($company, $request->all());


        Mail::to($this->settings->get("emails: new company"))->queue(new CreateCompany($request->all()));

        return response()->json([
            "status" => "ok",
            "company" => $company,
            "link" => $company->link,
        ]);
    }

    public function destroy(Request $request, $companyId)
    {
        $company = Company::findOrFail($companyId);

        if (Auth::user()->can("delete", $company)) {
            $company->delete();
        } else {
            return abort(403);
        }
    }

    public function validateData(CompanyRequest $request)
    {
        $improveList = [];
        $requestData = $request->all();

        if (count($requestData['images']) < 16) {
            $improveList[] = "разместите более 15 фото";
        }

        if (mb_strlen(strip_tags($requestData['info'])) < 250) {
            $improveList[] = "напишите более подробную информацию о компании";
        }

        if (!isset($requestData['contacts'])) {
            $improveList[] = "добавьте больше каналов для связи с клиентами - email, телефон, ссылка на сайт";
        }

        if (!isset($requestData['addresses']) || !count($requestData['addresses'])) {
            $improveList[] = "если у вашей компании имеется физический адрес - обязательно укажите его";
        }

        if (!isset($requestData['services']) || !count($requestData['services'])) {
            $improveList[] = "если ваша компания занимается оказанием услуг - обязательно укажите их";
        }


        return response()->json([
            "status" => "valid",
            "improveList" => $improveList
        ]);
    }


    private function setCompanyData(Company $company, array $requestData)
    {
        $company->name = $requestData['name'];
        $company->specialization = $requestData['specialization'];
        $company->phone = $requestData['phone'];
        $company->info = strip_tags($requestData['info'], "<p><br><ul><ol><li><b><u><i>");
        $company->images = $requestData['images'];


        $contactTypes = array_keys(Company::getContactTypesList());

        $contacts = [];
        if (isset($requestData['contacts'])) {
            foreach ($requestData['contacts'] as $contact) {
                if (in_array($contact['type'], $contactTypes) && (mb_strlen($contact['value']) > 0) && (mb_strlen($contact['value']) < 100)) {
                    $contacts[] = $contact;
                }
            }
        }
        $company->contacts = $contacts;


        $addresses = [];
        foreach ($requestData['addresses'] as $address) {
            if (
                array_key_exists("address", $address) &&
                array_key_exists("coordinates", $address) &&
                array_key_exists("info", $address)
            ) {
                $addresses[] = $address;
            }
        }
        $company->addresses = $addresses;


        $categoriesIds = CompanyCategory::whereIn("id", $requestData['categories'])
            ->where('active', true)
            ->get()
            ->pluck("id")
            ->toArray();

        $company->categories()->sync($categoriesIds);

        $services = [];
        $userSelectedServices = collect($requestData['services']);
        $servicesValidIdsList = App\Models\CompanyService::whereIn("id", $userSelectedServices->pluck("id")->toArray())
            ->where('active', true)
            ->get()
            ->pluck("id")
            ->toArray();


        foreach ($userSelectedServices as $service) {
            if (in_array($service['id'], $servicesValidIdsList)) {
                $services[$service['id']] = ["price" => $service['price']];
            }
        }

        $company->services()->sync($services);

        $company->save();
    }
}
