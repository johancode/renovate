<?php

namespace App\Http\Controllers\Api;

use App\Models\CompanyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class ServiceController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $searchTerm = $request->q;
        $page = $request->page;

        $services = CompanyService::orderBy("name")
            ->where("active", true)
            ->select(["name", "unit_type", "id"]);


        if ($searchTerm) {
            $services = $services->where('name', 'LIKE', '%' . $searchTerm . '%');
        }

        if ($request->ids) {
            $services = $services->whereIn('id', $request->ids);
        }


        $services = $services->paginate(7);


        return $services;
    }
}
