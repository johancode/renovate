<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Repositories\CompaniesRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $companiesRepository;

    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->companiesRepository = $companiesRepository;
    }


    public function showList(Request $request)
    {
        $searchQuery = htmlspecialchars($request->q);


        $companies = $this->companiesRepository->search($searchQuery, (int)$request->page);

        return view('search/pages/list')->with(compact('companies', 'searchQuery'));


//        if (!$q) {
//            $msg = "Поисковый запрос должен быть более 3х символов";
//            return view('search/pages/msg')->with(compact('msg', 'q'));
//        } else {
//            $companies = Company::where('active', true)
//                ->where('name', 'like', '%' . $q . '%')
//                ->paginate(10);
//
//            $companies->appends([
//                'q' => $q
//            ]);
//
//            if ($companies->isEmpty()) {
//                $msg = "По вашему запросу ничего не найдено";
//                return view('search/pages/msg')->with(compact('msg', 'q'));
//            } else {
//                return view('search/pages/list')->with(compact('companies', 'q'));
//            }
//        }
    }
}
