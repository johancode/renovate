<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class WelcomeController extends Controller
{
    function __construct()
    {
    }

    public function showWelcome(Request $request)
    {
        return view('welcome');
    }
}
