<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use App;

class PageController extends Controller
{
    public function showPage($url)
    {
        $page = Page::where('active', true)
            ->where('url', $url)
            ->firstOrFail();


        \SEOMeta::setTitle($page->title);
        \OpenGraph::setTitle($page->title);


        return view('simple-text')->with(compact("page"));
    }
}
