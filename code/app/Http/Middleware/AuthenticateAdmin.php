<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateAdmin
{
    private $allowRoles = [
        "admin",
        "manager",
    ];

    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response(trans('backpack::base.unauthorized'), 401);
            } else {
                return redirect()->guest(route('login'));
            }
        } else if (!Auth::guard($guard)->getUser()->hasAnyRole($this->allowRoles)) {
            return response("access denied", 401);
        }

        return $next($request);
    }
}
