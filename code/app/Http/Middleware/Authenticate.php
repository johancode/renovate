<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;

class Authenticate extends \Illuminate\Auth\Middleware\Authenticate
{
    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($guards);

        // add additional rule for user auth - field active must be TRUE

        if (!$this->auth->getUser()->active) {
            $this->auth->logout();

            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return response(view('auth.register_success'));
            }
        }

        return $next($request);
    }
}
