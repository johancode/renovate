<?php

namespace App\Http\ViewComposers;

use App;
use Illuminate\View\View;

class ProfileComposer
{
    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $view->with('sidebarMenu', $this->getProfileSidebarMenu());
    }

    private function getProfileSidebarMenu()
    {
        $sidebarMenuLinks = [];

        $addFn = function ($link, $title, $section) use (&$sidebarMenuLinks) {
            if (!isset($sidebarMenuLinks[$section])) {
                $sidebarMenuLinks[$section] = [];
            }

            $sidebarMenuLinks[$section][] = (object)[
                'link' => $link,
                'title' => $title,
                'is_current' => starts_with(url(request()->path()), $link),
            ];
        };


        $addFn(route('profile.companies.list'), 'Компании', 'Объявления');
//        $addFn(route('profile.user'), 'Редактировать профиль', 'Профиль');
//        $addFn(route('profile.password'), 'Смена пароля', 'Профиль');
//        $addFn(route('profile.notify'), 'Уведомления', 'Профиль');

        return $sidebarMenuLinks;
    }
}