<?php

namespace App\Http\ViewComposers;

use App;
use Auth;
use Illuminate\View\View;

class SiteComposer
{
    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $view->with('content', App::make('ContentHelper'));
        $view->with('settings', App::make('SettingsHelper'));

        $view->with('user', Auth::user());
    }
}