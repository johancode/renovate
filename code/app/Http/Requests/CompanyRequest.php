<?php

namespace App\Http\Requests;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CompanyRequest extends FormRequest
{
    public function authorize()
    {
        if (!Auth::check()) {
            return false;
        }

        switch (request()->action) {
            case  "update":
                $company = Company::whereId(request()->id)->firstOrFail();

                return Auth::user()->can("update", $company);
                break;

            case  "create":
                return Auth::user()->can("create", Company::class);
                break;
        }

        return false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'specialization' => 'required|max:255',
            'info' => 'required|max:3000|min:30',
            'categories' => 'required|array|max:20',
            'images' => 'required|array|between:6,30',
            'phone' => 'required|size:12',
            'contacts' => 'array|max:10',
            'addresses' => 'array|max:20',
            'services' => 'array|max:200',
        ];
    }

    public function messages()
    {
        return [
            'categories.max' => 'Не более :max',
            'categories.required' => 'Выберите как минимум один раздел для размещения',
            'images.required' => 'Выберите как минимум 6 фотографий для вашей комании',
            'images.between' => 'Количество фото должно быть не менее :min и не более :max',
            'contacts.max' => 'Слишком много контактной информации',
            'phone.size' => 'Телефон должен быть указан в формате +71234567890',
            'addresses.max' => 'Слишком много контактной информации',
            'services.max' => 'Слишком много информации об услугах',
        ];
    }
}
