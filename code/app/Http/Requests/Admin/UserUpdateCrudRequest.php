<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class UserUpdateCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required',
            'name'     => 'required',
            'password' => 'confirmed',
        ];
    }
}
