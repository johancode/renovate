<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class UserStoreCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|unique:'.config('laravel-permission.table_names.users', 'users').',email',
            'name'     => 'required',
            'password' => 'required|confirmed',
        ];
    }
}
