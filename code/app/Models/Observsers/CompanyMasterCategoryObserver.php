<?php

namespace App\Models\Observers;

use App\Models\CompanyMasterCategory;

class CompanyMasterCategoryObserver
{
    public function saving(CompanyMasterCategory $category)
    {
        if (!$category->url) {
            $category->url = str_slug($category->name);
        }

        $categoryOnUrl = CompanyMasterCategory::where('url', $category->url);

        if ($category->id) {
            $categoryOnUrl = $categoryOnUrl->where('id', '<>', $category->id);
        }


        if ($categoryOnUrl->count()) {
            $category->url .= time();
        }
    }

    public function updating(CompanyMasterCategory $category)
    {
    }
}