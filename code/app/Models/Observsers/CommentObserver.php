<?php

namespace App\Models\Observers;

use App\Events\CommentCreated;
use App\Events\CommentDeleted;
use App\Models\Comment;

class CommentObserver
{
    public function created(Comment $comment)
    {
        event(new CommentCreated($comment));
    }

    public function deleted(Comment $comment)
    {
        event(new CommentDeleted($comment));
    }
}