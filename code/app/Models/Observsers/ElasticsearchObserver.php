<?php

namespace App\Models\Observers;

use Elasticsearch\Client;

class ElasticsearchObserver
{
    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    private function getModelMap($model)
    {
        $map = @config('elasticsearch.mappings')[get_class($model)];

        if (!$map) {
            throw new \Exception("cant found map in conf file");
        }

        return new $map;
    }

    public function saved($model)
    {
        $map = $this->getModelMap($model);

        $this->elasticsearch->index([
            'index' => $map->getSearchIndex(),
            'type' => $map->getSearchType(),
            'id' => $model->id,
            'body' => $map->toSearchArray($model),
        ]);
    }

    public function deleted($model)
    {
        $map = $this->getModelMap($model);

        $this->elasticsearch->delete([
            'index' => $map->getSearchIndex(),
            'type' => $map->getSearchType(),
            'id' => $model->id,
        ]);
    }
}