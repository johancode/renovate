<?php

namespace App\Models\Observers;

use App\Events\CompanySaved;
use App\Models\Company;
use App;

class CompanyObserver
{
    public function saving(Company $company)
    {
        if (!$company->url) {
            $company->url = str_slug($company->name);
        }

        $companyOnUrl = Company::where('url', $company->url);

        if ($company->id) {
            $companyOnUrl = $companyOnUrl->where('id', '<>', $company->id);
        }


        if ($companyOnUrl->count()) {
            $company->url .= time();
        }
    }

    public function saved(Company $company)
    {
        event(new CompanySaved($company));
    }

    public function creating(Company $company)
    {
        $company->position_index = Company::max("position_index") + 1;
    }

    public function created(Company $company)
    {
        $company->threads()->firstOrCreate([
            'title' => $company->name
        ]);
    }

    public function updating(Company $company)
    {
    }

    public function deleting(Company $company)
    {
        $company->removeImages("images");
        $company->removeImages("logo");

        $company->getThread()->delete();
    }
}