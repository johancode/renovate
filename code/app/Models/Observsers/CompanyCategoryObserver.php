<?php

namespace App\Models\Observers;

use App\Models\CompanyCategory;

class CompanyCategoryObserver
{
    public function saving(CompanyCategory $category)
    {
        if (!$category->url) {
            $category->url = str_slug($category->name);
        }

        $categoryOnUrl = CompanyCategory::where('url', $category->url);

        if ($category->id) {
            $categoryOnUrl = $categoryOnUrl->where('id', '<>', $category->id);
        }

        if ($category->masterCategory) {
            $category->full_name = $category->masterCategory->name . ": " . $category->name;
        } else {
            $category->full_name = "- : " . $category->name;
        }


        if ($categoryOnUrl->count()) {
            $category->url .= time();
        }
    }

    public function updating(CompanyCategory $category)
    {
    }
}