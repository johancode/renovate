<?php

namespace App\Models\Observers;

use App\Models\Page;

class PageObserver
{
    public function saving(Page $page)
    {
        if (!$page->url) {
            $page->url = str_slug($page->title);
        }

        $pageOnUrl = Page::where('url', $page->url);

        if ($page->id) {
            $pageOnUrl = $pageOnUrl->where('id', '<>', $page->id);
        }


        if ($pageOnUrl->count()) {
            $page->url .= time();
        }
    }

    public function saved(Page $page)
    {
    }

    public function updating(Page $page)
    {
    }

    public function deleting(Page $page)
    {
    }
}