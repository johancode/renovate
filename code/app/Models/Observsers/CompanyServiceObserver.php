<?php

namespace App\Models\Observers;

use App\Models\CompanyService;

class CompanyServiceObserver
{
    public function saving(CompanyService $service)
    {
        if (!$service->url) {
            $service->url = str_slug($service->name);
        }

        $serviceOnUrl = CompanyService::where('url', $service->url);

        if ($service->id) {
            $serviceOnUrl = $serviceOnUrl->where('id', '<>', $service->id);
        }


        if ($serviceOnUrl->count()) {
            $service->url .= time();
        }
    }

    public function updating(CompanyService $company)
    {
    }
}