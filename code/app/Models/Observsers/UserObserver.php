<?php

namespace App\Models\Observers;

use App;
use Event;

class UserObserver
{
    public function creating($user)
    {
        $user->activation_hash = md5(uniqid());
    }
}