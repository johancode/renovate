<?php

namespace App\Models\Observers;

use App\Models\UserUploadImage;
use App;

class UserUploadImageObserver
{
    public function created(UserUploadImage $image)
    {
    }

    public function deleting(UserUploadImage $image)
    {
        $image->removeImages("image");
    }
}