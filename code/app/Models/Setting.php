<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Setting extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];
    public $timestamps = false;
}
