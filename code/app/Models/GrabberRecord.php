<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class GrabberRecord extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];
    protected $casts = [
        'content' => 'array',
    ];
}