<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait as BackpackCrud;
use App\Models\Traits\CompanyMutators;
use App\Models\Traits\ActiveScope;
use App\ElasticSearch\Traits\Searchable;
use JohanCode\ImageThumbs\ImageThumbsTrait;


class Company extends Model
{
    use BackpackCrud;
    use CompanyMutators;
    use ActiveScope;
    use ImageThumbsTrait;
    use Searchable;

    public static function getContactTypesList()
    {
        return [
            "site" => 'сайт',
            "phone" => 'телефон',
            "email" => 'электронная почта',
        ];
    }

    public static function getSourcesList()
    {
        return [
            0 => 'добавлено на сайте',
            1 => 'импорт с 2gis',
            2 => 'импорт с 124masters',
            3 => 'добавлено администратором'
        ];
    }

    public static function getImageThumbsParams($typeName)
    {
        $types = [
            'logo' => [
                'small' => [
                    'fit' => [50, 50, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
//                    'resize' => [
//                        50,
//                        50,
//                        function ($constraint) {
//                            $constraint->upsize();
//                            $constraint->aspectRatio();
//                        }
//                    ],
//                    'resizeCanvas' => [
//                        50,
//                        50,
//                        "center",
//                        false,
//                        "ffffff"
//                    ],
                ],
                'medium' => [
                    'fit' => [100, 100, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
                    'crop' => [100, 100],
                ],
            ],
            'images' => [
                'original' => [
                    'resize' => [1280, 800, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
                    'encode' => ['jpg'],
                ],
                'preview' => [
                    'fit' => [300, 300, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }, "top"],
                    'encode' => ['jpg'],
                ],
                'modal' => [
                    'resize' => [1280, 800, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
                    'encode' => ['jpg'],
//                    'insert' => [
//                        resource_path('assets/watermark.png'),
//                        'bottom-left',
//                        10,
//                        10
//                    ]
                ],
            ]
        ];

        return $types[$typeName];
    }

    protected $guarded = ['id'];
    protected $casts = [
        'contacts' => 'array',
        'logo' => 'array',
        'images' => 'array',
        'rate' => 'int',
        'active' => 'bool',
        'addresses' => 'array',
    ];

    public function services()
    {
        return $this
            ->belongsToMany('App\Models\CompanyService', 'ref_companies_services', 'company_id', 'service_id')
            ->withPivot('price');
    }

    public function categories()
    {
        return $this
            ->belongsToMany(\App\Models\CompanyCategory::class, 'ref_companies_categories', 'company_id', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }


    public function scopeWithServicePrice($query, $serviceId)
    {
        $query
            ->join('ref_companies_services', 'companies.id', '=', 'ref_companies_services.company_id')
            ->join('company_services', 'ref_companies_services.service_id', '=', 'company_services.id')
            ->where('ref_companies_services.service_id', '=', $serviceId);

        $query->select('companies.*', 'ref_companies_services.price as price', 'company_services.unit_type as unit_type');


        return $query;
    }


    public function threads()
    {
        return $this->morphMany(\App\Models\CommentThread::class, 'commentable');
    }

    public function getThread()
    {
        $thread = $this->threads()->first();

        if (!$thread) {
            $thread = $this->threads()->create([
                'title' => $this->name
            ]);
        }

        return $thread;
    }


    public function getContacts()
    {
        $contacts = [
            "phone" => [],
            "email" => [],
            "site" => [],
        ];


        if ($this->phone) {
            $contacts["phone"][] = $this->phone;
        }

        if ($this->contacts) {
            foreach ($this->contacts as $contact) {
                if (isset($contacts[$contact['type']])) {
                    $contacts[$contact['type']][] = $contact['value'];
                }
            }
        }

        return $contacts;
    }
}
