<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait as BackpackCrud;

class CommentThread extends Model
{
    use BackpackCrud;

    protected $guarded = ['id'];
    protected $casts = [
    ];

    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class, 'thread_id');
    }

    public function commentable()
    {
        return $this->morphTo();
    }
}
