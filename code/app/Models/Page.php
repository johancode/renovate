<?php

namespace App\Models;

use App\Models\Traits\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use JohanCode\ImageThumbs\ImageThumbsTrait;

class Page extends Model
{
    use CrudTrait;
    use ActiveScope;
    use ImageThumbsTrait;

    protected $guarded = ['id'];
    protected $casts = [
    ];
}
