<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JohanCode\ImageThumbs\ImageThumbsTrait;

class UserUploadImage extends Model
{
    use ImageThumbsTrait;

    public static function getImageThumbsParams($typeName)
    {
        $types = [
            'image' => [
                'original' => [
                    'resize' => [1280, 800, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
                    'encode' => ['jpg'],
                ],
                'preview' => [
                    'fit' => [200, 200, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
                    'crop' => [200, 200],
                    'encode' => ['jpg'],
                ],
            ],
        ];

        return $types[$typeName];
    }

    public function setImageAttribute($value)
    {
        $imageValue = $this->saveImageThumbsField('image', $value);
        $this->attributes['image'] = json_encode($imageValue);
    }

    public function getImageAttribute()
    {
        return $this->getImageValue('image');
    }


    protected $guarded = ['id'];
    protected $casts = [
        'image' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}
