<?php

namespace App\Models\Traits;


trait CompanyMutators
{
    public function getServicesListAttribute()
    {
        $services = $this
            ->services()
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item->id => [
                    "price" => $item->pivot->price
                ]];
            })
            ->toArray();


        return $services;
    }

    public function setServicesListAttribute($newValue)
    {
        $services_data = json_decode($newValue, 1);
        if (!$services_data) {
            $services_data = [];
        }


        if ($services_data) {
            $this->services()->sync($services_data);
        } else {
            $this->services()->detach();
        }
    }

    public function getAdminDebugHelp()
    {
        $item = 1;
        return "<span>" . $item . "</span>";
    }


    public function setLogoAttribute($value)
    {
        $logoValue = $this->saveImageThumbsField('logo', $value);
        $this->attributes['logo'] = json_encode($logoValue);
    }

    public function getLogoAttribute()
    {
        return $this->getImageValue('logo');
    }


    public function setImagesAttribute($value)
    {
        $imagesValue = $this->saveImageThumbsField('images', $value, true);
        $this->attributes['images'] = json_encode($imagesValue);
    }

    public function getImagesAttribute()
    {
        return $this->getImageValue('images');
    }


    public function getLinkAttribute()
    {
        return url(route('companies.company', $this->url));
    }
}