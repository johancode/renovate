<?php

namespace App\Models\Traits;


trait ActiveScope
{
    public function scopeActive($query)
    {
        $query->where($this->getTable() . '.active', true);
        return $query;
    }
}