<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class CompanyMasterCategory extends Model
{
    use CrudTrait;

    public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = [];


    public function categories()
    {
        return $this->belongsToMany(\App\Models\CompanyCategory::class, 'master_category_id');
    }
}
