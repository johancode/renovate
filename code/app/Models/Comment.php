<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait as BackpackCrud;

class Comment extends Model
{
    use BackpackCrud;

    protected $guarded = ['id'];
    protected $casts = [
    ];

    public static function getSourcesList()
    {
        return [
            0 => 'добавлено пользователем на сайте',
            1 => 'импортированно вручную',
            2 => 'импорт с flamp',
        ];
    }

    public function thread()
    {
        return $this->belongsTo(\App\Models\CommentThread::class, 'thread_id');
    }

//    public function user()
//    {
//        return $this->belongsTo(\App\User::class, 'user_id');
//    }
}
