<?php

namespace App\Models;

use App\Models\Traits\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CompanyCategory extends Model
{
    use CrudTrait;
    use ActiveScope;

    public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = [
    ];

    public function companies()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'ref_companies_categories', 'category_id', 'company_id');
    }

    public function services()
    {
        return $this->belongsToMany(\App\Models\CompanyService::class, 'ref_companies_categories', 'category_id', 'service_id');
    }

    public function masterCategory()
    {
        return $this->belongsTo(\App\Models\CompanyMasterCategory::class, 'master_category_id');
    }

    public function getLinkAttribute()
    {
        return url(route('companies.category', $this->url));
    }
}
