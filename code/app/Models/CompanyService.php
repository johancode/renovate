<?php

namespace App\Models;

use App\Models\Traits\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CompanyService extends Model
{
    use CrudTrait;
    use ActiveScope;

    public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = [
    ];


    public function companies()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'ref_companies_services', 'service_id', 'company_id');
    }

    public function categories()
    {
        return $this->belongsToMany(\App\Models\CompanyCategory::class, 'ref_services_categories', 'service_id', 'category_id');
    }

    public function getLinkAttribute()
    {
        return url(route('companies.service', $this->url));
    }
}
