<?php

namespace App\ElasticSearch\Traits;

use App\Models\Observers\ElasticsearchObserver;

trait Searchable
{
    public static function bootSearchable()
    {
        // TODO: Testing queue
        if (config('services.search.enabled')) {
            static::observe(app(ElasticsearchObserver::class));
        }
    }
}