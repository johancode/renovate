<?php

namespace App\ElasticSearch\Mappings;

class CompanyMap extends Map
{
    protected $map = [
        '_source' => [
            'enabled' => true
        ],
        'properties' => [
            'name' => [
                'type' => 'string',
                'analyzer' => self::RU_ANALYZER_NAME
            ],
            'info' => [
                'type' => 'text',
                'analyzer' => self::RU_ANALYZER_NAME
            ],
            'specialization' => [
                'type' => 'string',
                'analyzer' => self::RU_ANALYZER_NAME
            ],
            'user_rate' => [
                'type' => 'integer',
            ],
            'active' => [
                'type' => 'boolean',
            ],
            'position_index' => [
                'type' => 'integer',
            ],
            'position_score' => [
                'type' => 'integer',
            ],
            'user_id' => [
                'type' => 'integer',
            ],
            'contacts' => [
                'type' => 'nested',
                'properties' => [
                    'type' => [
                        'type' => 'string',
                        'index' => 'not_analyzed'
                    ],
                    'value' => [
                        'type' => 'string',
                        'index' => 'not_analyzed'
                    ]
                ],
            ],
            'addresses' => [
                'type' => 'nested',
                'properties' => [
                    'address' => [
                        'type' => 'string',
                        'index' => 'not_analyzed'
                    ],
                    'coordinates' => [
                        'type' => 'string',
                        'index' => 'not_analyzed'
                    ]
                ],
            ],
            'categories' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                        'index' => 'not_analyzed'
                    ],
                    'name' => [
                        'type' => 'string',
                        'analyzer' => self::RU_ANALYZER_NAME
                    ],
                    'url' => [
                        'type' => 'string',
                        'index' => 'not_analyzed'
                    ]
                ],
            ],
            'images_count' => [
                'type' => 'integer'
            ],
            'comments_count' => [
                'type' => 'integer'
            ],
            'services_count' => [
                'type' => 'integer'
            ]
        ]
    ];

    protected $ESAttachedFields = [
        'images_count',
        'comments_count',
        'services_count',
    ];


    public function toSearchArray($item)
    {
        $result = [];

        $result['id'] = $item->id;

        $result['name'] = $item->name;
        $result['info'] = strip_tags($item->info);
        $result['specialization'] = $item->specialization;
        $result['user_rate'] = $item->user_rate;
        $result['active'] = $item->active;
        $result['position_score'] = $item->position_score;
        $result['position_index'] = $item->position_index;
        $result['user_id'] = $item->user_id;
        $result['addresses'] = $item->addresses;
        $result['contacts'] = $item->contacts;

        $result['images_count'] = $item->images ? count($item->images) : 0;
        $result['comments_count'] = $item->getThread()->comments->count();
        $result['services_count'] = $item->services->count();

        $result['categories'] = $item
            ->categories()
            ->get()
            ->map(function ($item) {
                return [
                    "id" => $item->id,
                    "name" => $item->name,
                    "url" => $item->url,
                ];
            })
            ->toArray();


        return $result;
    }
}