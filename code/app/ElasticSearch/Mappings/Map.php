<?php

namespace App\ElasticSearch\Mappings;

use App;
use App\ElasticSearch\Traits\Searchable;

abstract class Map
{
    protected $model;
    protected $searchIndex;
    protected $searchType;
    protected $esClient;
    protected $ESAttachedFields = [];

    const RU_ANALYZER_NAME = "ru_analyzer";

    public function __construct()
    {
        $this->esClient = App::make("Elasticsearch\Client");
        $this->prepareModel();
    }

    protected $settings = [
        'analysis' => [
            'analyzer' => [
                self::RU_ANALYZER_NAME => [
                    "filter" => [
                        'lowercase',
                        'russian_morphology',
                        'english_morphology',
                        'my_stopwords'
                    ],
                    'type' => 'custom',
                    'tokenizer' => 'standard',
                ],
            ],
            'filter' => [
                'my_stopwords' => [
                    'type' => 'stop',
                    'stopwords' => 'а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я,a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with',
                ],
            ],
        ]
    ];

    protected function prepareModel()
    {
        $this->model = collect(config('elasticsearch.mappings'))
            ->filter(function ($value, $key) {
                return $value == get_class($this);
            })
            ->keys()
            ->first();

        if (!$this->model) {
            throw new \Exception('model property should be filled');
        }

        $this->model = new $this->model();
        $traits = class_uses_recursive(get_class($this->model));

        if (!isset($traits[Searchable::class])) {
            throw new \Exception(get_class($this->model) . ' does not use the searchable trait');
        }

        $this->searchIndex = $this->model->getTable();
        $this->searchType = $this->model->getTable();
    }

    public function removeIndex()
    {
        if (!$this->esClient->indices()->exists(['index' => $this->index])) {
            $this->esClient->indices()->delete(['index' => $this->index]);
        }
    }

    public function getMapSettings()
    {
        return [
            'index' => $this->searchIndex,
            'body' => [
                'settings' => $this->settings,
                'mappings' => [
                    $this->searchType => $this->map
                ]
            ]
        ];
    }

    /**
     * Gets model search index name
     *
     * @return string
     */
    public function getSearchIndex(): string
    {
        return $this->searchIndex;
    }

    /**
     * Gets model search type name
     *
     * @return string
     */
    public function getSearchType(): string
    {
        return $this->searchType;
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * Gets attached fields name list
     *
     * @return array
     */
    public function getESAttachedFields(): array
    {
        return $this->ESAttachedFields;
    }
}