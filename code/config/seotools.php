<?php

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults' => [
            'title' => 'Поиск строительных и ремонтных услуг: HouseRepair.info',
            'description' => 'Поиск заказов и подрядчиков, строительных компаний, ремонтных услуг в Красноярске - адреса, цены, отзывы, справочная информация',
            'separator' => ' - ',
            'keywords' => [],
            'canonical' => false,
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google' => null,
            'bing' => null,
            'alexa' => null,
            'pinterest' => null,
            'yandex' => null,
        ],
    ],
    'opengraph' => [
        'defaults' => [
            'title' => 'HouseRepair.info',
            'description' => 'Поиск заказов и подрядчиков, строительных компаний, ремонтных услуг в Красноярске - адреса, цены, отзывы, справочная информация',
            'url' => false,
            'type' => false,
            'site_name' => false,
            'images' => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
];
