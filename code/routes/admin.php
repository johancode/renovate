<?php

Route::get('/logout', function () {
    \Auth::logout();
    return redirect()->route('home');
});

Route::get('/company-dublicates', function () {
    $companies = DB::table("companies")
        ->select(DB::raw('count(*) as count, name'))
        ->where('active', true)
        ->where('moderated', false)
        ->groupBy('name')
        ->having('count', '>', 1)
        ->limit(1000)
        ->get();

    foreach ($companies as $company) {
        echo $company->name . "<br>";
    }
    exit();
});

Route::get('/company-with-comments', function () {
    $items = DB::select("SELECT
  comment_threads.title,
  count(comments.thread_id),
  comments.thread_id
FROM comment_threads

  JOIN comments ON comment_threads.id = comments.thread_id
  JOIN companies
    ON companies.id = comment_threads.commentable_id

WHERE companies.moderated = FALSE
GROUP BY thread_id;");

    foreach ($items as $item) {
        echo $item->title . "<br>";
    }
    exit();
});


Route::get('api/service', 'Api\ServiceController@index');
Route::post('api/service/parse', 'Api\ServiceController@parse');
Route::get('api/user', 'Api\UserController@index');
Route::get('api/user/{id}', 'Api\UserController@show');
Route::get('api/c-thread', 'Api\CommentThreadController@index');
Route::get('api/c-thread/{id}', 'Api\CommentThreadController@show');


CRUD::resource('permission', 'PermissionCrudController');
CRUD::resource('role', 'RoleCrudController');
CRUD::resource('user', 'UserCrudController');

CRUD::resource('company', 'CompanyCrudController');
CRUD::resource('company-category', 'CompanyCategoryCrudController');
CRUD::resource('company-master-category', 'CompanyMasterCategoryCrudController');
CRUD::resource('service', 'CompanyServiceCrudController');
CRUD::resource('grabber-record', 'GrabberRecordCrudController');

CRUD::resource('setting', 'SettingCrudController');
CRUD::resource('text-block', 'TextBlockCrudController');
CRUD::resource('page', 'PageCrudController');

CRUD::resource('c-thread', 'CommentThreadCrudController');
CRUD::resource('comment', 'CommentCrudController');