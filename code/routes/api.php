<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(
    [
        'as' => 'api.',
    ],
    function () {
        Route::apiResources([
            'companies' => 'CompanyController',
            'comment' => 'CommentController',
        ]);

        Route::post('/companies/validate', [
            'uses' => 'CompanyController@validateData',
        ]);

        Route::group([
            'prefix' => 'company-form'
        ], function () {
            Route::get('/categories', [
                'uses' => 'ProfileController@getCategories',
            ]);
        });

        Route::post('upload', 'UploadController@upload');
        Route::get('service', 'ServiceController@index');
    }
);
