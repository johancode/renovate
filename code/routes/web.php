<?php

Auth::routes();
Route::view('/register/success', 'auth.register_success');
Route::get('/register/{hash}', [
    'as' => 'user_activation',
    'uses' => 'Auth\RegisterController@activateUser',
])->where('hash', '[0-9a-w]{32}');


Route::get('/', 'WelcomeController@showWelcome')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');


Route::group(
    [
        'as' => 'companies.',
        'prefix' => 'companies',
    ],
    function () {
        Route::get('/', [
            'as' => 'list',
            'uses' => 'CompaniesController@showCompaniesList',
        ]);

        Route::get('/services', [
            'as' => 'services',
            'uses' => 'CompaniesController@showServices',
        ]);

        Route::get('/services/{name}', [
            'as' => 'service',
            'uses' => 'CompaniesController@showCompaniesByService',
        ]);

        Route::get('/categories', [
            'as' => 'categories',
            'uses' => 'CompaniesController@showCategories',
        ]);

        Route::get('/categories/{name}', [
            'as' => 'category',
            'uses' => 'CompaniesController@showCompaniesByCategory',
        ]);

        Route::get('/{name}', [
            'as' => 'company',
            'uses' => 'CompaniesController@showCompany',
        ]);
    }
);


Route::group(
    [
        'as' => 'profile.',
        'prefix' => 'profile',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/', [
            'as' => 'dashboard',
            'uses' => 'ProfileController@showDashboard',
        ]);


        Route::group(
            [
                'as' => 'companies.',
                'prefix' => 'companies',
            ],
            function () {
                Route::get('/', [
                    'as' => 'list',
                    'uses' => 'ProfileController@showCompaniesList',
                ]);

                Route::get('/create', [
                    'as' => 'create-form',
                    'uses' => 'ProfileController@showCompanyCreateForm',
                ]);

                Route::get('/{company_id}/edit', [
                    'as' => 'edit-form',
                    'uses' => 'ProfileController@showCompanyEditForm',
                ])->where('company_id', '[0-9]+');
            }
        );


        Route::get('/user', [
            'as' => 'user',
            'uses' => 'ProfileController@showUserForm',
        ]);
//        Route::post('/user', [
//            'as' => 'user',
//            'uses' => 'ProfileController@updateUser',
//        ]);

        Route::get('/password', [
            'as' => 'password',
            'uses' => 'ProfileController@showUserPasswordForm',
        ]);
//        Route::post('/password', [
//            'as' => 'password',
//            'uses' => 'ProfileController@updateUserPassword',
//        ]);

        Route::get('/notify', [
            'as' => 'notify',
            'uses' => 'ProfileController@showUserNotifyForm',
        ]);
//        Route::post('/notify', [
//            'as' => 'notify',
//            'uses' => 'ProfileController@updateUserNotify',
//        ]);
    }
);


Route::get('/search', [
    'as' => 'search',
    'uses' => 'SearchController@showList',
]);


Route::get('{url}', [
    'uses' => 'PageController@showPage'
])->where('url', '^((?!_debugbar)(?!admin).)*');
