$(function () {

    var adminUrl = "/" + window.location.pathname.split("/")[1] + "/api/service";


    var initSelectTwo = function ($item) {
        $item.select2({
            placeholder: "не выбрано",
            ajax: {
                url: adminUrl,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: $.map(data.data, function (item) {
                            return {
                                text: item.name.substring(0, 50) + " (" + item.unit_type + ")",
                                slug: item.name,
                                id: item.id
                            }
                        }),
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
        });
    };


    var saveData = function ($source, $items) {
        var data = {};

        $items.each(function () {
            var $row = $(this),
                serviceId = $row.find("select").val(),
                price = $row.find("input").val();

            if (serviceId && price) {
                data[serviceId] = {
                    price: price
                };
            }
        });

        $source.val(JSON.stringify(data));
    };


    var addNewServiceItem = function ($widget, service, price) {
        $widget
            .find(".js-company-service-widget__add")
            .trigger("click");

        var $item = $widget.find(".js-company-service-widget__item:last"),
            $selectedOptioin = $("<option selected value='" + service.id + "'>" +
                service.name.substring(0, 50) +
                " (" + service.unit_type + ")</option>");

        $item
            .find("select")
            .select2('destroy')
            .append($selectedOptioin);


        initSelectTwo($item.find("select"));

        $item.find("input").val(price);
    };


    $(".js-company-service-widget").each(function () {
        var $widget = $(this),
            $newItemTemplate = $widget.find(".js-company-service-widget__template"),
            $source = $widget.find(".js-company-service-widget__source").find("input");

        $widget
            .find(".js-company-service-widget__add")
            .on("click", function () {
                var $newItem = $newItemTemplate
                    .clone()
                    .show()
                    .addClass("js-company-service-widget__item")
                    .removeClass("js-company-service-widget__template");


                $newItem
                    .find("input")
                    .on("change, keyup", function () {
                        saveData(
                            $source,
                            $widget.find(".js-company-service-widget__item")
                        );
                    });

                $newItem
                    .find("select")
                    .on("change", function () {
                        saveData(
                            $source,
                            $widget.find(".js-company-service-widget__item")
                        );
                    });


                $newItem.insertAfter($widget.find("tr:last"));
                initSelectTwo($newItem.find("select"));

                $newItem
                    .find(".js-company-service-widget__remove")
                    .on("click", function () {
                        $newItem.find("select").select2('destroy');
                        $newItem.remove();

                        saveData(
                            $source,
                            $widget.find(".js-company-service-widget__item")
                        );
                    });
            });


        // load old data
        if ($source.val()) {
            var data = JSON.parse($source.val());

            var ids = [];
            for (var id in data) {
                ids.push(id);
            }


            if (ids.length) {
                $.ajax({
                    url: adminUrl,
                    data: {
                        ids: ids,
                        limit: 400,
                    },
                    success: function (response) {
                        response.data.forEach(function (service) {
                            var price = 0;

                            for (var service_id in data) {
                                if (service_id == service.id) {
                                    price = data[service_id].price;
                                }
                            }


                            addNewServiceItem(
                                $widget,
                                {
                                    name: service.name,
                                    id: service.id,
                                    unit_type: service.unit_type
                                },
                                price
                            );
                        });
                    }
                });
            }
        }


        var $parseInput = $widget.find(".js-parse-by-text__input"),
            $parseButton = $widget.find(".js-parse-by-text__btn");

        $parseButton.on("click", function (e) {
            e.preventDefault();
            var data = $parseInput.val();

            $.ajax({
                url: adminUrl + "/parse",
                data: {
                    services: data
                },
                method: "POST",
                success: function (response) {
                    response.forEach(function (item) {
                        addNewServiceItem(
                            $widget,
                            {
                                name: item.variants[0].name,
                                id: item.variants[0].id,
                                unit_type: item.variants[0].unit_type
                            },
                            item.price
                        );
                    });

                    saveData(
                        $source,
                        $widget.find(".js-company-service-widget__item")
                    );
                }
            });
        });

        $widget.find(".js-company-service-widget__show-parse").on("click", function (e) {
            e.preventDefault();
            $widget.find(".js-company-service-widget__parse-box").show();
        });
    });
});