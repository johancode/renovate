require('./bootstrap');
require('./partials/icons');


import Vue from 'vue';

import store from './store/index';

import burgerMenu from './../components/burger-menu.vue';
import gallery from './../components/gallery.vue';
import limitHeightText from './../components/limit-height-text.vue';
import modalbox from './../components/modalbox.vue';
import commentsWidget from './../components/comments-widget.vue';
import contactPhone from './../components/contacts/phone.vue';
import contactUrl from './../components/contacts/url.vue';
import contactEmail from './../components/contacts/email.vue';
import companyForm from './../components/company-form.vue';
import companyShareBox from './../components/company-share-box.vue';
import companyPreview from './../components/profile/company-preview.vue';
import companyList from './../components/company/company-list.vue';
import companyFilter from './../components/company/filter.vue';


let renovateApp = {};

if (document.getElementById('app') !== null) {
    let renovateApp = new Vue({
        el: '#app',
        store,
        components: {
            gallery,
            limitHeightText,
            modalbox,
            commentsWidget,
            contactPhone,
            contactUrl,
            burgerMenu,
            contactEmail,
            companyForm,
            companyShareBox,
            companyPreview,
            companyList,
            companyFilter,
        },
        methods: {
            runModalBox(links, options) {
                let vue = this;
                vue.$refs.modalBox.runGallery(links, options);
            },
        },
        mounted() {
        }
    });
}

export {renovateApp};