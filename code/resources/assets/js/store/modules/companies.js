import companiesApi from "../../api/companies";

export default {
    namespaced: true,
    state: {
        items: [],
        pagination: null,
        waitingList: [],
        filter: {},
        categoriesTree: null,
        pageTitle: "",
        pageSubTitle: ""
    },
    getters: {
        isWaiting(state, getters) {
            return state.waitingList.length > 0;
        },
        filterCategory(state) {
            return state.filter.category;
        },
    },
    actions: {
        fetchCompanies({commit, dispatch, state}) {
            dispatch('addWaitingAction', 'companies-list');
            commit('clearCompanies');
            commit('setPageTitle', "");
            commit('setPageSubTitle', "");

            dispatch('loadNextPage', 'companies-list');
        },
        loadNextPage({commit, state, dispatch}) {
            return new Promise((resolve, reject) => {
                dispatch('addWaitingAction', 'companies-list');

                companiesApi
                    .getCompanies({
                        ...state.filter,
                        page: state.pagination ? (state.pagination.currentPage + 1) : 1
                    })
                    .then(function (companiesData) {
                        commit('pushCompanies', companiesData.data);
                        commit('setCategoriesTree', companiesData.filters.categoriesTree);
                        commit('setPagination', {
                            currentPage: companiesData.meta.current_page,
                            nextUrl: companiesData.links.next,
                            balance: Math.max(0, companiesData.meta.total - (companiesData.meta.per_page * companiesData.meta.current_page))
                        });


                        let currentMasterCategory = companiesData.filters.categoriesTree.find(masterCategory => {
                            return masterCategory.categories.find(category => category.id == state.filter.category);
                        });

                        if (currentMasterCategory) {
                            let currentCategory = currentMasterCategory.categories.find(category => category.id == state.filter.category);

                            commit('setPageTitle', currentCategory.name);
                            commit('setPageSubTitle', currentCategory.subtitle);
                        }


                        dispatch('removeWaitingAction', 'companies-list');
                        resolve();
                    })
                    .catch(function (error) {
                        console.log(error);
                        dispatch('removeWaitingAction', 'companies-list');
                        reject();
                    });
            });
        },
        addWaitingAction({commit, state}, actionName) {
            if (!state.waitingList.find(_actionName => _actionName == actionName)) {
                commit('pushWaitingAction', actionName);
            }
        },
        removeWaitingAction({commit, state}, actionName) {
            if (state.waitingList.find(_actionName => _actionName == actionName)) {
                commit('pullWaitingAction', actionName);
            }
        },
        setFilterParams({commit, dispatch}, newFilterValue) {
            commit('setFilter', newFilterValue);
            commit('clearCompanies');
            dispatch('fetchCompanies');
        },
        setFilterParam({commit, dispatch, state}, filterParam) {
            let newFilterValue = {
                ...state.filter,
                ...filterParam
            };

            commit('setFilter', newFilterValue);
            commit('clearCompanies');
            dispatch('fetchCompanies');
        },
    },
    mutations: {
        clearCompanies(state) {
            state.items = [];
        },
        setCompanies(state, companies) {
            state.items = companies;
        },
        pushCompanies(state, companies) {
            companies.forEach(company => {
                state.items.push(company);
            });
        },
        setPagination(state, pagination) {
            state.pagination = pagination;
        },
        pushWaitingAction(state, actionName) {
            state.waitingList.push(actionName);
        },
        pullWaitingAction(state, actionName) {
            state.waitingList = state.waitingList.filter(_actionName => _actionName !== actionName);
        },
        setFilter(state, filter) {
            state.filter = filter;
            state.filter.withFilters = true;
            if (state.pagination) {
                state.pagination = null;
            }
        },
        setCategoriesTree(state, categories) {
            state.categoriesTree = categories;
        },
        setPageTitle(state, title) {
            state.pageTitle = title;
        },
        setPageSubTitle(state, subTitle) {
            state.pageSubTitle = subTitle;
        },
    }
};