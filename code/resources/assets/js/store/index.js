import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

import companies from './modules/companies';

export default new Vuex.Store({
    modules: {
        companies,
    },
    state: {},
    getters: {},
    mutations: {}
});