import axios from 'axios';
import ROUTES from '../partials/routes';


export default {
    getCompanies(filtersList) {
        return new Promise((resolve, reject) => {
            axios
                .get(ROUTES.companies, {
                    params: {
                        ...filtersList,
                        withFilters: true
                    }
                })
                .then(function (response) {
                    resolve(response.data);
                })
                .catch(function (error) {
                    reject(error);
                });
        });
    }
};