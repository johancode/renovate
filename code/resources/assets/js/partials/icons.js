import '../../svg/user-auth.svg';
import '../../svg/user-reg.svg';
import '../../svg/catalog.svg';

import '../../svg/catalog/category-brush.svg';
import '../../svg/catalog/category-cart.svg';
import '../../svg/catalog/category-engineer.svg';
import '../../svg/catalog/category-water.svg';
import '../../svg/catalog/category-light.svg';
import '../../svg/catalog/category-heater.svg';
import '../../svg/catalog/category-windows.svg';
import '../../svg/catalog/category-door.svg';
import '../../svg/catalog/category-sofa.svg';
import '../../svg/catalog/category-drawing.svg';
import '../../svg/catalog/category-shovel.svg';
import '../../svg/catalog/category-fan.svg';
import '../../svg/catalog/category-anvil.svg';
import '../../svg/catalog/category-wheelbarrow.svg';
import '../../svg/catalog/category-woodcutter.svg';
import '../../svg/catalog/category-safety.svg';
import '../../svg/catalog/category-truck.svg';
import '../../svg/catalog/category-brick.svg';
import '../../svg/catalog/category-roof.svg';
import '../../svg/catalog/category-sauna.svg';
import '../../svg/catalog/category-drill.svg';
import '../../svg/catalog/category-swiss.svg';
import '../../svg/catalog/category-trowel.svg';

import '../../svg/contacts/phone.svg';
import '../../svg/contacts/url.svg';
import '../../svg/contacts/insta.svg';
import '../../svg/contacts/vk.svg';
import '../../svg/contacts/fb.svg';
import '../../svg/contacts/mail.svg';

import '../../svg/forms/upload-image.svg';
import '../../svg/forms/remove-image.svg';
import '../../svg/forms/add-contact.svg';
import '../../svg/forms/map-point.svg';
import '../../svg/forms/map-remove.svg';
import '../../svg/forms/save-company.svg';
import '../../svg/forms/right-arrow.svg';
import '../../svg/forms/left-arrow.svg';
import '../../svg/forms/idea.svg';

import '../../svg/share/share-fb.svg';
import '../../svg/share/share-ok.svg';
import '../../svg/share/share-twi.svg';
import '../../svg/share/share-vk.svg';

import '../../svg/categories-filter/service.svg';
import '../../svg/categories-filter/shop.svg';
import '../../svg/categories-filter/filter-search.svg';

import '../../svg/company/company-comments.svg';
import '../../svg/company/company-images.svg';
import '../../svg/company/company-services.svg';
