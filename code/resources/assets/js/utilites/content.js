const decline = (amount, zero, one, two, many) => {
    const declineReg = /%n/g;

    const nmod10 = amount % 10;
    const nmod100 = amount % 100;

    if (!amount) return zero.replace(declineReg, amount);
    if ((amount == 1) || (nmod10 == 1 && nmod100 != 11)) return one.replace(declineReg, amount);
    if (nmod10 > 1 && nmod10 < 5 && nmod100 != 12 && nmod100 != 13 && nmod100 != 14) return two.replace(declineReg, amount);

    return many.replace(declineReg, amount);
};

export {
    decline
};
