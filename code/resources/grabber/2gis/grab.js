var ids = [],
    process = true;

var grab = function() {
    if (!process){
        return false;
    }

    var items = document.getElementsByClassName("miniCard__headerTitleLink"),
        reg = /firm\/([\d]+)\?/i;

    for (var i = 0, max = items.length; i < max; i++) {
        var result = items[i].getAttribute("href").match(reg);
        if (result && result[1]) {
            ids.push(result[1]);
        }
    }

    console.log("grab - " + ids.length);


    var $nextPageLink = $(".pagination__arrow._right");
    if ($nextPageLink.hasClass("_disabled")) {
        process = false;
        console.log("finished");

        var data = JSON.stringify(ids);
        var url = 'data:text/json;charset=utf8,' + encodeURIComponent(data);
        window.open(url, '_blank');
        window.focus();

    }else{
        $nextPageLink.trigger("click");
        setTimeout(grab, 600);
    }
};

grab();