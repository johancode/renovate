@extends('layouts.app')
@section('content')

    <div class="page__content__bg container">
        <div class="columns">
            <div class="column is-8 is-offset-2">
                <div class="content">


                    <h1 class="is-1 title">
                        {{ $page->title }}
                    </h1>


                    <div class="content">
                        {!! $page->content !!}
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection