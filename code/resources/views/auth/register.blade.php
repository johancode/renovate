@extends('layouts.app')
@section('content')

    <div class="columns is-centered">
        <div class="column is-6-desktop is-4-widescreen form-register__wrap">

            <h1 class="is-4 subtitle">
                Регистрация на сайте
            </h1>


            <div class="form-register">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="field">
                        <label class="label">
                            Электронная почта
                        </label>
                        <div class="control">
                            <input
                                    value="{{ old('email') }}"
                                    class="input {{ $errors->has('email') ? 'is-danger' : '' }}"
                                    type="text"
                                    name="email">
                        </div>

                        @if ($errors->has('email'))
                            <p class="help is-danger">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">
                            Имя
                        </label>
                        <div class="control">
                            <input
                                    value="{{ old('name') }}"
                                    name="name"
                                    class="input {{ $errors->has('name') ? 'is-danger' : '' }}"
                                    type="text">
                        </div>

                        @if ($errors->has('name'))
                            <p class="help is-danger">
                                {{ $errors->first('name') }}
                            </p>
                        @endif
                    </div>

                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label class="label">
                                    Пароль
                                </label>
                                <div class="control">
                                    <input
                                            class="input {{ $errors->has('password') ? 'is-danger' : '' }}"
                                            name="password"
                                            type="password">
                                </div>
                            </div>

                            @if ($errors->has('password'))
                                <p class="help is-danger">
                                    {{ $errors->first('password') }}
                                </p>
                            @endif
                        </div>
                        <div class="column">
                            <div class="field">
                                <label class="label">
                                    Пароль, еще раз
                                </label>
                                <div class="control">
                                    <input
                                            class="input"
                                            name="password_confirmation"
                                            type="password">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="agree">
                                Я принимаю условия <a href="/rules">Пользовательского соглашения и политикой обработки
                                    персональных данных</a>
                            </label>
                        </div>

                        @if ($errors->has('agree'))
                            <p class="help is-danger">
                                {{ $errors->first('agree') }}
                            </p>
                        @endif
                    </div>


                    <div class="field">
                        <div class="control">
                            <button class="button is-primary" type="submit">
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


@endsection
