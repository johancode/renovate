@extends('layouts.app')
@section('content')

    <div class="columns">
        <div class="column is-6 is-offset-3 form-register__wrap">

            <h1 class="is-4 subtitle">
                Регистрация на сайте
            </h1>

            <p>
                Процесс регистрации на сайте практически завершён.<br>
                На указанный Вами адрес электронной почты было отправлено письмо с ссылкой для активации учетной записи.
            </p>

        </div>
    </div>


@endsection
