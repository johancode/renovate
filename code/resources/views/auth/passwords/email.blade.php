@extends('layouts.app')
@section('content')


    <div class="columns is-centered">
        <div class="column is-6-tablet is-5-desktop is-4-widescreen form-login__wrap">

            <h1 class="is-4 subtitle">
                Восстановление пароля
            </h1>


            <div class="form-login__info">
                <p>
                    Укажите электронную почту указанную вами при регистрации
                </p>
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @else

                <form class="" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="field">
                        <label class="label">
                            Электронная почта
                        </label>
                        <div class="control">
                            <input
                                    value="{{ old('email') }}"
                                    name="email"
                                    class="input {{ $errors->has('email') ? 'is-danger' : '' }}"
                                    type="text"
                                    autofocus>
                        </div>

                        @if ($errors->has('email'))
                            <p class="help is-danger">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>


                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-primary" type="submit">
                                Отправить
                            </button>
                        </div>
                    </div>
                </form>
            @endif


        </div>
    </div>


@endsection
