@extends('layouts.app')
@section('content')


    <div class="columns is-centered">
        <div class="column is-6-tablet is-5-desktop is-4-widescreen form-login__wrap">

            <h1 class="is-4 subtitle">
                Восстановление пароля
            </h1>


            <form class="" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="field">
                    <label class="label">
                        Электронная почта
                    </label>
                    <div class="control">
                        <input
                                value="{{ $email or old('email') }}"
                                name="email"
                                class="input {{ $errors->has('email') ? 'is-danger' : '' }}"
                                type="text"
                                autofocus>
                    </div>

                    @if ($errors->has('email'))
                        <p class="help is-danger">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>


                <div class="field">
                    <label class="label">
                        Пароль
                    </label>
                    <div class="control">
                        <input
                                name="password"
                                class="input {{ $errors->has('password') ? 'is-danger' : '' }}"
                                type="password">
                    </div>

                    @if ($errors->has('password'))
                        <p class="help is-danger">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
                </div>


                <div class="field">
                    <label class="label">
                        Пароль, еще раз
                    </label>
                    <div class="control">
                        <input
                                class="input {{ $errors->has('password_confirmation') ? 'is-danger' : '' }}"
                                name="password_confirmation"
                                type="password">
                    </div>

                    @if ($errors->has('password_confirmation'))
                        <p class="help is-danger">
                            {{ $errors->first('password_confirmation') }}
                        </p>
                    @endif
                </div>


                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-primary" type="submit">
                            Сбросить пароль
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>


@endsection
