@extends('layouts.app')
@section('content')

    <div class="columns">
        <div class="column is-6 is-offset-3 form-register__wrap">

            <h1 class="is-4 subtitle">
                Регистрация на сайте
            </h1>

            <p>
                Ваш адрес успешно подтвержден.
                Благодарим за регистрацию на нашем сайте
            </p>

        </div>
    </div>


@endsection
