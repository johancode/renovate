@extends('layouts.app')
@section('content')


    <div class="columns is-centered">
        <div class="column is-6-tablet is-5-desktop is-4-widescreen form-login__wrap">

            <h1 class="is-4 subtitle">
                Авторизация на сайте
            </h1>

            <div class="form-login">
                <form class="" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="field">
                        <label class="label">
                            Электронная почта
                        </label>
                        <div class="control">
                            <input
                                    value="{{ old('email') }}"
                                    name="email"
                                    class="input {{ $errors->has('email') ? 'is-danger' : '' }}"
                                    type="text"
                                    autofocus>
                        </div>

                        @if ($errors->has('email'))
                            <p class="help is-danger">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">
                            Пароль
                        </label>
                        <div class="control">
                            <input
                                    name="password"
                                    class="input {{ $errors->has('password') ? 'is-danger' : '' }}"
                                    type="password">
                        </div>

                        @if ($errors->has('password'))
                            <p class="help is-danger">
                                {{ $errors->first('password') }}
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <div class="buttons">

                            <button class="button is-primary" type="submit">
                                Войти
                            </button>
                            <label class="button is-text">
                                <input type="checkbox" style="margin-right: 5px">
                                Запомнить меня
                            </label>
                        </div>
                    </div>
                </form>
            </div>


            <div class="login-additional-links">
                <div class="columns is-mobile">
                    <div class="column is-narrow">
                        <a href="{{ route('password.request') }}">
                            Я забыл свой пароль
                        </a>
                    </div>
                    <div class="column">
                        <a href="{{ route('register') }}">
                            Регистрация
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
