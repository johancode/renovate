@extends('layouts.app')
@section('content')

    <section class="hero is-fullheight">

        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-10-desktop is-12-tablet is-9-widescreen is-9-fullhd">

                        <div class="welcome">
                            <div class="welcome__description">
                                <div class="title">
                                    house<span>repair</span>.info
                                </div>

                                <h1 class="subtitle">
                                    Поиск строительных и ремонтных услуг в Красноярске
                                </h1>

                                <p>
                                    На нашем сайте вы найдете нужную компанию для решения любой ремонтной или
                                    строительной задачи.
                                    <br>
                                    Мы собрали все компании Красноярска на одном ресурсе для удобного поиска наиболее
                                    выгодных предложений.
                                </p>

                                <p>&nbsp;</p>

                                <a href="{{ route('companies.list') }}" class="button is-outlined is-dark is-small">
                                    Компании
                                </a>

                                <a href="{{ route('companies.categories') . "?type=shops" }}"
                                   class="button is-outlined is-dark is-small">
                                    Товары и материалы
                                </a>

                                <a href="{{ route('companies.categories') . "?type=services" }}"
                                   class="button is-outlined is-dark is-small">
                                    Мастера и сервисы
                                </a>

                                <a href="{{ route('companies.services') }}" class="button is-outlined is-dark is-small">
                                    цены на услуги
                                </a>
                            </div>

                            <div class="welcome__contacts">
                                <div class="welcome__contacts__item">
                                    <a href="{{ route('profile.companies.list') }}" class="button is-info">
                                        <span class="icon">
                                            <svg class="navbar__link-icon">
                                                <use xlink:href="#catalog"></use>
                                            </svg>
                                        </span>

                                        <span class="is-hidden-mobile">
                                            добавить информацию о своей компании
                                        </span>

                                        <span class="is-hidden-tablet">
                                            добавить свою компанию
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection