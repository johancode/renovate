@extends('emails.parts.base')
@section('content')

    @include('emails.parts.header', ['title' => 'Регистрация на сайте'])
    @include('emails.parts.text', ['text' => 'Ваша компания добавлена на сайт HOUSEREPAIR.INFO в автоматическом режиме<br>Для авторизации используйте следующие данные:'])
    @include('emails.parts.text', ['text' => "Электронная почта: {$userEmail} <br>Пароль: {$userPass}"])
    @include('emails.parts.button', ['title' => 'Перейти в личный кабинет', 'link' => $loginLink])
    @include('emails.parts.subinfo', ['text' => 'Письмо отправлено автоматически. Если вы считаете, что получили его по ошибке, просто проигнорируйте его.'])

@endsection