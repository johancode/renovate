@extends('emails.parts.base')
@section('content')

    @include('emails.parts.header', ['title' => 'Восстановление пароля'])
    @include('emails.parts.text', ['text' => 'Для восстановления пароля на сайте HOUSEREPAIR.INFO перейдите по ссылке:'])
    @include('emails.parts.button', ['title' => 'Сменить пароль', 'link' => $token])
    @include('emails.parts.text', ['text' => 'Если ссылка не работает, скопируйте ее вручную:<br>' . $token])
    @include('emails.parts.subinfo', ['text' => 'Письмо отправлено автоматически. Если вы считаете, что получили его по ошибке, просто проигнорируйте его.'])

@endsection