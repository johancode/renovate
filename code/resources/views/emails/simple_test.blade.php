@extends('emails.parts.base')
@section('content')

    @include('emails.parts.header', ['title' => 'Заголовок'])
    @include('emails.parts.text', ['text' => 'Текст сообщения в письме. Какая-то важая информация<br>Есть возможность переносить строки'])
    @include('emails.parts.button', ['title' => 'Кнопка', 'link' => 'http://ya.ru'])
    @include('emails.parts.subinfo', ['text' => 'Доп инфа'])

@endsection