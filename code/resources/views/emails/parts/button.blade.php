<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 500px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
         class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
            <!--[if (mso)|(IE)]>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="background-color:transparent;" align="center">
                        <table cellpadding="0" cellspacing="0" border="0" style="width: 500px;">
                            <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

            <!--[if (mso)|(IE)]>
            <td align="center" width="500"
                style=" width:500px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                valign="top"><![endif]-->
            <div class="col num12"
                 style="min-width: 320px;max-width: 500px;display: table-cell;vertical-align: top;">
                <div style="background-color: transparent; width: 100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                        <!--<![endif]-->


                        <div align="center" class="button-container center"
                             style="padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;">
                            <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                   style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td style="padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;"
                                        align="center">
                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                     xmlns:w="urn:schemas-microsoft-com:office:word" href=""
                                                     style="height:38px; v-text-anchor:middle; width:212px;"
                                                     arcsize="11%" strokecolor="#53709F"
                                                     fillcolor="#53709F">
                                            <w:anchorlock/>
                                            <center style="color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:14px;">
                            <![endif]-->
                            <div style="color: #ffffff; background-color: #53709F; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 212px; width: 172px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none;">
                                            <span style="font-size:12px;line-height:24px;"><a
                                                        href="{{ $link }}"
                                                        style="font-size: 14px; line-height: 28px; color: #fff; text-decoration: none; display: block"
                                                        data-mce-style="font-size: 14px;">{{ $title }}</a></span>
                            </div>
                            <!--[if mso]></center></v:roundrect></td></tr></table><![endif]-->
                        </div>


                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>