@extends('emails.parts.base')
@section('content')

    @include('emails.parts.header', ['title' => 'Заявка на добавление новой компании'])
    @include('emails.parts.text', ['text' => $text])

@endsection