<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('layouts.parts.head')
<body>


<div class="page">
    <div class="page__content">
        @yield('content')
    </div>
</div>


@include('layouts.parts.scripts')
</body>

</html>