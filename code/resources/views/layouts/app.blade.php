<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('layouts.parts.head')


<body>

@if(request()->path() == "/")
    @yield('content')
@else
    <div class="page" id="app">
        <div class="page__nav">
            @include('layouts.parts.top-nav')
        </div>

        <div class="page__content">
            @yield('content')
        </div>

        @include('layouts.parts.footer')
    </div>
@endif


@stack('js-scripts')
@include('layouts.parts.scripts')
</body>

</html>