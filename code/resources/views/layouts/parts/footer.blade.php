<footer class="footer">
    <div class="container">
        <div class="columns">
            <div class="column is-narrow">
                <strong class="footer__site-name">
                    houserepair.info © {{ date("Y") }}
                </strong>

                <ul>
                    <li>
                        <a href="">
                            Условия использования
                        </a>
                    </li>
                    <li>
                        <a href="mailto:help@houserepair.info">
                            help@houserepair.info
                        </a>
                    </li>
                </ul>
            </div>

            <div class="column is-hidden-mobile">
                <div class="columns is-desktop">
                    <div class="column is-narrow footer__categories">
                        {!! $content->getText("footer: links block #1") !!}
                    </div>

                    <div class="column is-narrow footer__categories">
                        {!! $content->getText("footer: links block #2") !!}
                    </div>
                </div>
            </div>


            <div class="column is-4">
                <form action="{{ route('search') }}" class="footer__search-form">
                    <div class="field has-addons">
                        <div class="control is-expanded">
                            <input class="input is-small" name="q" type="text"
                                   placeholder="Название компании или услуги">
                        </div>
                        <div class="control">
                            <button type="submit" class="button is-info is-small">
                                поиск
                            </button>
                        </div>
                    </div>
                </form>

                <p>
                    Информация носит исключительно ознакомительный характер и не является публичной офертой.
                </p>
            </div>
        </div>
    </div>
</footer>