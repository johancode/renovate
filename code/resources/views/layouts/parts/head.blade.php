<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link href="/static/build/styles.css?v=5" rel="stylesheet">


    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#e8d100">
    <meta name="theme-color" content="#ffffff">


    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}

    <link href="https://fonts.googleapis.com/css?family=Exo+2:400,400i,700,700i|Oswald:700" rel="stylesheet">
    <meta name="google-site-verification" content="prH8O9k9vhGa8iybRWLdNLmqQm44k4QO4Xahlt6ESbw"/>
    <meta name="csrf-token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" id="token">


    <style>
        [v-cloak] {
            display: none;
        }
    </style>
</head>