<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="container">

        <div class="navbar-brand">
            <a class="navbar-item" href="{{ url("/") }}">
                <span class="navbar__logo">
                    house<span>repair</span>.info
                </span>
            </a>

            <burger-menu v-cloak>
                <div slot="main-links">
                    <a class="navbar-item" href="{{ route('companies.list') }}">
                        компании
                    </a>
                    <a class="navbar-item" href="{{ route('companies.categories') }}">
                        разделы
                    </a>
                    <a class="navbar-item" href="{{ route('companies.services') }}">
                        услуги
                    </a>
                </div>

                <div slot="subitems">
                    @if(auth()->check())
                        <a href="" class="">
                            {{ auth()->user()->name }}
                        </a>

                        <a href="{{ route('logout') }}"
                           class=""
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            выход
                        </a>
                    @else
                        <a class="button is-small is-primary is-outlined" href="{{ route('login') }}">
                            <span class="icon">
                                <svg class="navbar__link-icon">
                                    <use xlink:href="#user-auth"></use>
                                </svg>
                            </span>

                            <span>
                                Вход
                            </span>
                        </a>


                        <a class="button is-small is-primary is-outlined" href="{{ route('register') }}">
                            <span class="icon">
                                <svg class="navbar__link-icon">
                                    <use xlink:href="#user-reg"></use>
                                </svg>
                            </span>

                            <span>
                                Регистрация
                            </span>
                        </a>
                    @endif
                </div>
            </burger-menu>
        </div>


        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="{{ route('companies.list') }}">
                    компании
                </a>
                <a class="navbar-item" href="{{ route('companies.categories') }}">
                    разделы
                </a>
                <a class="navbar-item" href="{{ route('companies.services') }}">
                    услуги
                </a>
            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    @if(auth()->check())
                        <a href="" class="navbar__user-name">
                            {{ auth()->user()->name }}
                        </a>

                        <a href="{{ route('logout') }}"
                           class="navbar__logout"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            выход
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @else
                        <div class="field is-grouped">


                            <p class="control">
                                <a class="button is-small is-primary is-outlined" href="{{ route('login') }}">
                                    <span class="icon">
                                        <svg class="navbar__link-icon">
                                            <use xlink:href="#user-auth"></use>
                                        </svg>
                                    </span>

                                    <span>
                                        Вход
                                    </span>
                                </a>
                            </p>

                            <p class="control">
                                <a class="button is-small is-primary is-outlined" href="{{ route('register') }}">
                                    <span class="icon">
                                        <svg class="navbar__link-icon">
                                            <use xlink:href="#user-reg"></use>
                                        </svg>
                                    </span>

                                    <span>
                                        Регистрация
                                    </span>
                                </a>
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</nav>