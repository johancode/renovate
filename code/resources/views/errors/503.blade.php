@extends('layouts.critical-error')
@section('content')

    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">

                <h1 class="title is-1">
                    503 Error
                </h1>
                <h2 class="subtitle">
                    The server is overloaded or down for maintenance. Please try again later.
                </h2>

            </div>
        </div>
    </section>

@endsection