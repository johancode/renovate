@extends('layouts.critical-error')
@section('content')

    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">

                <h1 class="title is-1">
                    Ошибка 500
                </h1>
                <h2 class="subtitle">
                    Данная страница сейчас недоступна
                </h2>

            </div>
        </div>
    </section>

@endsection