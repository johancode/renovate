@extends('layouts.app')
@section('content')

    <section class="hero is-medium">
        <div class="hero-body">
            <div class="container has-text-centered">

                <h1 class="title is-1">
                    Ошибка 404
                </h1>
                <h2 class="subtitle">
                    Страница не найдена
                </h2>

            </div>
        </div>
    </section>

@endsection