@extends('layouts.app')
@section('content')


    <div class="container two-col-content">
        <div class="columns">
            <div class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('companies.blocks.sidebar-add-company')
{{--                @include('companies.blocks.sidebar-categories-links')--}}
            </div>


            <div class="column is-9 two-col-content__section">

                <h1 class="is-4 subtitle">
                    {{ $service->name }}
                </h1>


                <div class="service">
                    @if(!$companies->isEmpty())
                        <div class="service__description">
                            <p>
                                Компании оказывающие данную услугу:
                            </p>
                        </div>


                        <div class="service__companies">
                            @foreach($companies as $company)
                                @include('companies.blocks.company-preview', ['company' => $company])
                            @endforeach

                            {{ $companies->links() }}
                        </div>

                    @else
                        <p>
                            К сожалению компаний занимающихся данной услугой не найдено.
                        </p>
                    @endif
                </div>

            </div>
        </div>
    </div>


@endsection