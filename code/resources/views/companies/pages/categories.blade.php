@extends('layouts.app')
@section('content')

    <div class="page__content__bg page__content__bg_gray container categories-list">
        <div class="columns is-multiline is-desktop">
            <div class="column">
                <h1 class="is-4 list-title list-title_white">
                    {{ $pageTitle }}
                </h1>
            </div>
            <div class="column is-narrow">
                <div class="categories-list__filter">
                    <div class="field has-addons">
                        <p class="control">
                            <a class="button is-outlined is-warning is-rounded @if($filterOptions['shops']['active']) is-active @endif"
                               href="{{ $filterOptions['shops']['link'] }}">
                                <svg class="">
                                    <use xlink:href="#shop"></use>
                                </svg>
                                <span class="is-hidden-mobile">
                                    продажа товаров
                                </span>
                                <span class="is-flex-mobile is-hidden-tablet">
                                    товары
                                </span>
                            </a>
                        </p>
                        <p class="control">
                            <a class="button is-outlined is-warning is-rounded @if($filterOptions['services']['active']) is-active @endif"
                               href="{{ $filterOptions['services']['link'] }}">
                                <svg class="">
                                    <use xlink:href="#service"></use>
                                </svg>
                                <span class="is-hidden-mobile">
                                    оказание услуг
                                </span>
                                <span class="is-flex-mobile is-hidden-tablet">
                                    услуги
                                </span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="columns is-multiline">

            @foreach($categoriesList as $masterCategory)
                <div class="column is-half-tablet is-half-desktop is-one-third-widescreen is-one-quarter-fullhd master-category__wrap">
                    <div class="master-category">

                        <div class="master-category__header">
                            <div class="master-category__header__icon">
                                <svg>
                                    <use xlink:href="#category-{{ $masterCategory->icon_label }}"></use>
                                </svg>
                            </div>

                            <div class="master-category__header__info">
                                <h2 class="list-subtitle is-6 master-category__header__info__title">
                                    {{ $masterCategory->name }}
                                </h2>

                                <div class="master-category__header__info__score">
                                    {{ $masterCategory->companies_score }} {{ $content->declension($masterCategory->companies_score, "", "компания", "компании", "компаний") }}
                                </div>
                            </div>
                        </div>

                        <div class="master-category__links">
                            @foreach($masterCategory->categories as $category)
                                <div>
                                    <a href="{{ $category->link }}">
                                        {{ $category->name }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>


@endsection