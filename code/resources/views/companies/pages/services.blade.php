@extends('layouts.app')
@section('content')


    <div class="page__content__bg container">
        <h1 class="is-4 subtitle">
            Все услуги компаний
        </h1>

        @if(!$services->isEmpty())
            <div class="columns">
                <?php
                $servicesList = $services->chunk(ceil($services->count() / 2));
                ?>


                @foreach($servicesList as $services)
                    <div class="column">
                        @foreach($services as $service)
                            <div>
                                <a href="{{ $service->link }}">
                                    {{ $service->name }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach

            </div>
        @endif
    </div>


@endsection