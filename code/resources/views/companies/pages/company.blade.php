@extends('layouts.app')
@section('content')


    <div class="container two-col-content">
        <div class="columns">
            <div class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('companies.blocks.sidebar-add-company')
                {{--                @include('companies.blocks.sidebar-categories-links')--}}
                @include('companies.blocks.sidebar-share')
            </div>


            <div class="column is-9 two-col-content__section">
                <div>
                    <div class="company">
                        <div class="company__header">
                            <div class="columns">
                                {{--@if($company->logo)--}}
                                {{--<div class="column is-narrow">--}}
                                {{--<img src="{{ $company->logo->small }}" alt="{{ $company->name }}"--}}
                                {{--title="{{ $company->name }}">--}}
                                {{--</div>--}}
                                {{--@endif--}}

                                <div class="column">
                                    <h1 class="is-1 title">
                                        {{ $company->name }}
                                    </h1>

                                    @if($company->specialization)
                                        <h2 class="is-4 subtitle">
                                            {{ $company->specialization }}
                                        </h2>
                                    @endif
                                </div>

                                <div class="column is-narrow">
                                    {{--<strong>rate:</strong> {{ $company->rate }}--}}
                                </div>
                            </div>
                        </div>
                    </div>


                    @if(!$categories->isEmpty())
                        <div class="company__categories">
                            @foreach($categories as $category)
                                <a href="{{ $category->link }}" class="tag is-primary">
                                    {{ $category->name }}
                                </a>
                            @endforeach
                        </div>
                    @endif


                    <div class="company__info">
                        @if($company->addresses)
                            <div class="company__addresess">

                                @foreach($company->addresses as $address)
                                    <div>
                                        {{ $address['address'] }}
                                    </div>
                                @endforeach

                            </div>
                        @endif


                        <div class="company__contacts">
                            <div class="tags">

                                @foreach($contacts['phone'] as $contact)
                                    <contact-phone>
                                        {{ $content->encodePhone($contact) }}
                                    </contact-phone>
                                @endforeach

                                @foreach($contacts['email'] as $contact)
                                    <contact-email>
                                        {{ $contact }}
                                    </contact-email>
                                @endforeach

                                @foreach($contacts['site'] as $contact)
                                    <contact-url>
                                        {{ $contact }}
                                    </contact-url>
                                @endforeach

                            </div>
                        </div>
                    </div>


                    <div class="columns">
                        @if($company->info)
                            <div class="column">
                                <div class="content">
                                    <limit-height-text :height-limit="315">
                                        {!! $content->prepareUserText($company->info) !!}
                                    </limit-height-text>
                                </div>
                            </div>
                        @endif

                        @if($company->images)
                            <div class="column">
                                <div class="company__images">

                                    <gallery v-on:img-click="runModalBox">
                                        <div slot="images" style="display: none;">
                                            @foreach($company->images as $img)
                                                <a href="{{ $img->modal }}" title="{{ $company->name }}">
                                                    <img
                                                            src="{{ $img->preview }}"
                                                            alt="{{ $company->name }}"
                                                            title="{{ $company->name }}">
                                                </a>
                                            @endforeach
                                        </div>
                                    </gallery>

                                </div>
                            </div>
                        @endif
                    </div>


                    <comments-widget
                            user-name="{{ 'Sam Jackson' }}"
                            user-avatar-src="{{ url("/static/images/user-plug.png") }}"
                            thread-id="{{ $thread->id }}"
                            request-uri="{{ route('api.comment.index') }}">

                        <div slot="title">
                            <div class="header-title title is-5">
                                Отзывы о компании «{{ $company->name }}»
                            </div>
                        </div>


                        <div>
                            @if(!$comments->isEmpty())
                                <ul>
                                    @foreach($comments as $comment)
                                        <li>
                                            <strong>
                                                {{ $comment->author_name }}
                                            </strong>

                                            {{ $comment->text }}
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                    </comments-widget>


                    @if(count($services))
                        <div class="company__prices">
                            <h4 class="subtitle is-4">
                                Услуги компании
                            </h4>

                            <div class="columns">

                                <?php
                                $servicesList = $services->chunk(ceil($services->count() / 2));
                                ?>


                                @foreach($servicesList as $services)
                                    <div class="column">
                                        @foreach($services as $service)
                                            <div class="company__prices__item">
                                                <div class="columns">
                                                    <div class="column">
                                                        <a href="{{ $service->link }}">
                                                            {{ $service->name }}
                                                            ({{ $service->unit }})
                                                        </a>
                                                    </div>

                                                    <div class="column is-narrow">
                                                        {{ $service->price }} &#8381;
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    @endif

                    <modalbox ref="modalBox"></modalbox>
                </div>
            </div>
        </div>
    </div>


    @if($showShareBox)
        <company-share-box
                company-specialization="{{ $company->specialization }}"
                company-name="{{ $company->name }}"
                company-link="{{ $company->link }}">
        </company-share-box>
    @endif


@endsection

@push("js-scripts")
    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="//yastatic.net/share2/share.js"></script>
@endpush