@extends('layouts.app')
@section('content')

    <div class="container two-col-content companies-list">
        <div class="columns">
            <aside class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('companies.blocks.filter')
                @include('companies.blocks.category-info')
                @include('companies.blocks.sidebar-add-company')
            </aside>


            <section class="column is-9 two-col-content__section">
                <div class="companies-list__title">
                    <h1 class="list-title">
                        {{ $pageTitle }}
                    </h1>

                    <div class="list-subtitle">
                        @if(isset($pageSubTitle))
                            {{ $pageSubTitle }}
                        @endif
                    </div>
                </div>

                <company-list :initial-category="{{ isset($category) ? $category->id : 0 }}">
                </company-list>
            </section>
        </div>
    </div>


@endsection