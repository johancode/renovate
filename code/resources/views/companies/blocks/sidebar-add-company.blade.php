<div class="sidebar-box">

    <div class="title is-5">
        Добавить компанию
    </div>

    <p>
        Деятельность вашей компании связана с ремонтом или строительством?
        <br>
        Ждем вашу компанию у нас на сайте!
        <br>
        Размещение бесплатно.
    </p>

    <a class="button is-small is-primary" href="{{ route('profile.companies.create-form') }}">
        <span class="icon">
            <svg class="navbar__link-icon">
                <use xlink:href="#catalog"></use>
            </svg>
        </span>

        <span>
            Добавить <span class="is-hidden-tablet-only is-hidden-desktop-only">свою </span>компанию
        </span>
    </a>

</div>