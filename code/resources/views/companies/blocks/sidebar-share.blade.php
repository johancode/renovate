<div class="sidebar-box is-hidden-mobile" ref="share">
    <div class="title is-5">
        Социальные сети
    </div>

    <p>
        Рассказать о компании «{{ $company->name }}» вашим друзьям:
    </p>

    <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,telegram"></div>
</div>
