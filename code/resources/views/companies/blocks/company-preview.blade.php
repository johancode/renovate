<div class="company-preview">
    <div class="columns">
        <div class="column">
            <a href="{{ $company->link }}" class="company-preview__title">
                {{ $company->name }}
            </a>

            <div class="company-preview__specialization">
                @if($company->specialization)
                    {{ $company->specialization }}
                @endif
            </div>
        </div>

        <div class="column is-narrow">
            {{ $company->images_count }} фото |
            {{ $company->comments_count }} комметов |
            {{ $company->services_count }} услуг

            @if($company->price)
                <span class="title is-6">
                    {{ $company->price }} руб./ {{ $company->unit_type }}
                </span>
            @endif
        </div>
    </div>


    @if($company->addresses)
        <div class="company-preview__addresses">

            @foreach($company->addresses as $address)
                <div>
                    {{ $address['address'] }}
                </div>
            @endforeach

        </div>
    @endif
</div>


