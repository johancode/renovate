@if(isset($category) && ($category->seo_title || $category->seo_text))

    <div class="sidebar-box">
        <h2 class="title is-5">
            {{ $category->seo_title }}
        </h2>

        {!! $category->seo_text !!}
    </div>

@endif