<aside class="menu user-profile-menu">

    @foreach($sidebarMenu as $sectionTitle => $sectionItems)
        <p class="menu-label">
            {{ $sectionTitle }}
        </p>

        <ul class="menu-list">
            @foreach($sectionItems as $item)
                <li>
                    <a href="{{ $item->link }}" class="@if($item->is_current) is-active @endif">
                        {{ $item->title }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endforeach

    <ul class="menu-list menu-list-exit">
        <li>
            <a href="">
                Выход
            </a>
        </li>
    </ul>
</aside>