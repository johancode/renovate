<div class="user-sidebar">
    <div class="user-sidebar__avatar">
        <img src="http://renovate.local/static/images/user-plug.png">
    </div>

    <div class="user-sidebar__info">
        <div class="user-sidebar__info__name">
            {{ $user->name }}
        </div>

        <div class="user-sidebar__info__text">
            {{ $user->email }}
        </div>
    </div>
</div>