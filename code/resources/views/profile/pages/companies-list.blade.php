@extends('layouts.app')
@section('content')

    <div class="container two-col-content">
        <div class="columns">
            <div class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('profile.blocks.user-info')
                @include('profile.blocks.menu')
            </div>


            <div class="column is-9 two-col-content__section">

                <a href="{{ route('profile.companies.create-form') }}" class="button is-success">
                    ДОБАВИТЬ НОВУЮ КОМПАНИЮ
                </a>
                <br>
                <br>
                <br>


                @if($companies->isEmpty())
                    Компаний нет
                @else
                    @foreach($companies as $company)
                        <?php
                        $companyData = [
                            "id" => $company->id,
                            "link" => $company->link,
                            "name" => $company->name,
                            "specialization" => $company->specialization,
                            "created" => $company->created_at->diffForHumans(),
                        ];
                        ?>

                        <company-preview :company="{{ json_encode($companyData) }}">
                        </company-preview>
                    @endforeach

                    {{ $companies->links() }}
                @endif
            </div>
        </div>
    </div>


@endsection