@extends('layouts.app')
@section('content')


    <div class="container two-col-content">
        <div class="columns">
            <div class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('profile.blocks.user-info')
                @include('profile.blocks.menu')
            </div>


            <div class="column is-9 two-col-content__section company-form-page">
                <h1 class="is-1 title">
                    Редактирование компании
                </h1>

                <h2 class="is-2">
                    {{ $company->name }}
                </h2>


                <div class="company-form-page__divider"></div>


                <company-form
                        mode="update"
                        :update-company-id="{{ $company->id }}"
                ></company-form>


                <div class="columns">
                    <div class="column is-12">

                        <div class="company-form-page__help-text">
                            <p>
                                Объявление должно соответствовать <a href="/advert-rules" target="_blank">правилами
                                    публикации</a> информации на сайте <a href="https://houserepair.info"
                                                                          target="_blank">houserepair.info</a>.
                                <br>
                                Не подавайте одно и то же объявление повторно.
                                <br>
                                Не указывайте телефон, электронную почту или адрес сайта в описании или на фото.
                            </p>

                            <p>
                                Важно: заказчики при прочих равных выбирают тех мастеров, которые сообщили о себе больше
                                информации.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection