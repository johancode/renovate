@extends('layouts.app')
@section('content')

    <div class="container two-col-content">
        <div class="columns">
            <div class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('profile.blocks.user-info')
                @include('profile.blocks.menu')
            </div>


            <div class="column is-9 two-col-content__section">
                user-password-form
            </div>
        </div>
    </div>


@endsection