@extends('layouts.app')
@section('content')

    <div class="container two-col-content">
        <div class="columns">
            <div class="column is-3 two-col-content__sidebar is-hidden-mobile">
                @include('profile.blocks.user-info')
                @include('profile.blocks.menu')
            </div>


            <div class="column is-9 two-col-content__section">


                <div class="timeline">
                    {{--<header class="timeline-header">--}}
                        {{--<span class="tag is-small is-success">online</span>--}}
                    {{--</header>--}}

                    <div class="timeline-item is-success">
                        <div class="timeline-marker is-success"></div>
                        <div class="timeline-content">
                            <p class="heading">January 2016</p>
                            <p>Timeline content - Can include any HTML element</p>
                        </div>
                    </div>

                    <div class="timeline-item is-success">
                        <div class="timeline-marker is-success is-icon">
                            <i>bil</i>
                        </div>
                        <div class="timeline-content">
                            <p class="heading">Март 2017</p>
                            <p>Добавление подписки «pro»</p>
                        </div>
                    </div>

                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <p class="heading">
                                {{ $user->created_at->diffForHumans() }}
                            </p>
                            <p>
                                Регистрация на сайте
                            </p>
                            <p>
                                ip: 129.123.0.2
                                <br>
                                email: {{ $user->email }}
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


@endsection