@if ($paginator->hasPages())


    <nav class="pagination is-small" role="navigation" aria-label="pagination">

        @if ($paginator->onFirstPage())
            <a disabled class="pagination-previous">Назад</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="pagination-previous">Назад</a>
        @endif


        @if ($paginator->hasMorePages())
            <a class="pagination-next" href="{{ $paginator->nextPageUrl() }}">Вперед</a>
        @else
            <a class="pagination-next" disabled>Вперед</a>
        @endif


        <ul class="pagination-list">

            @foreach ($elements as $element)
                @if (is_string($element))
                    <li>
                        <span class="pagination-ellipsis">&hellip;</span>
                    </li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li>
                                <a class="pagination-link is-current" aria-label="Страница {{ $page }}"
                                   aria-current="page">{{ $page }}</a>
                            </li>
                        @else
                            <li>
                                <a class="pagination-link" href="{{ $url }}"
                                   aria-label="Страница {{ $page }}">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

        </ul>
    </nav>


@endif
