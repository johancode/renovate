@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}"
                         class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">


                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}">
                        <i class="fa fa-dashboard">
                        </i>
                        <span>{{ trans('backpack::base.dashboard') }}</span>
                    </a>
                </li>

                <li class="header">
                    КАТАЛОГ
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder-o"></i>
                        <span>Компании и услуги</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/company') }}">
                                <i class="fa fa-space-shuttle"></i>
                                <span>Компании</span>
                            </a>
                        </li>

                        <li>
                            <a target="_blank"
                               href="{{ url(config('backpack.base.route_prefix', 'admin').'/company-dublicates') }}">
                                <i class="fa fa-copy"></i>
                                <span>Компании, дубликаты</span>
                            </a>
                        </li>

                        <li>
                            <a target="_blank"
                               href="{{ url(config('backpack.base.route_prefix', 'admin').'/company-with-comments') }}">
                                <i class="fa fa-reply"></i>
                                <span>С отзывами, без модерации</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/company-master-category') }}">
                                <i class="fa fa-folder-open"></i>
                                <span>Разделы</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/company-category') }}">
                                <i class="fa fa-puzzle-piece"></i>
                                <span>Категории</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/service') }}">
                                <i class="fa fa-plug"></i>
                                <span>Услуги</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/grabber-record') }}">
                        <i class="fa fa-globe"></i>
                        <span>
                            Grabber
                        </span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-comments"></i>
                        <span>Комментарии</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/c-thread') }}">
                                <i class="fa fa-comments-o"></i>
                                <span>Темы</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/comment') }}">
                                <i class="fa fa-comment"></i>
                                <span>Комментарии</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="header">
                    КОНЕТЕНТ
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/text-block') }}">
                        <i class="fa fa-font"></i>
                        <span>
                            Текстовые блоки
                        </span>
                    </a>
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/page') }}">
                        <i class="fa fa-file-text"></i>
                        <span>
                            Страницы
                        </span>
                    </a>
                </li>


                <li class="header">
                    ОПЦИИ
                </li>


                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}">
                        <i class="fa fa-gear"></i>
                        <span>
                            Настройки
                        </span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i
                                        class="fa fa-user"></i> <span>Users</span></a>
                        </li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i
                                        class="fa fa-group"></i> <span>Roles</span></a></li>
                        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i
                                        class="fa fa-key"></i> <span>Permissions</span></a></li>
                    </ul>
                </li>


                <!-- ======================================= -->
                <li class="header">{{ trans('backpack::base.user') }}</li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i
                                class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
@endif
