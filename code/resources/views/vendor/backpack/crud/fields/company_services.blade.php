<div class="js-company-service-widget">

    <div @include('crud::inc.field_wrapper_attributes') >
        <label>{!! $field['label'] !!}</label>

        <div class="js-company-service-widget__source">
            <input
                    type="text"
                    style="display: none;"
                    name="{{ $field['name'] }}"
                    value="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? json_encode($field['value']) : (isset($field['default']) ? $field['default'] : '' )) }}"
                    @include('crud::inc.field_attributes')
            >
        </div>


        <div class="array-container form-group">


            <div class="company-service-widget">
                <table class="table table-bordered table-striped m-b-0">

                    <thead>
                    <tr>
                        <th style="font-weight: 600!important;">
                            Название
                        </th>
                        <th style="font-weight: 600!important;">
                            Цена
                        </th>

                        <th></th>
                    </tr>
                    </thead>

                    <tbody class="table-striped">
                    <tr class="array-row js-company-service-widget__template" style="display: none;">
                        <td>
                            <select class="select-two js-company-service-widget__service">
                            </select>
                        </td>

                        <td class="company-service-widget__col-2">
                            <input class="form-control input-sm js-company-service-widget__price"
                                   placeholder="0"
                                   type="text">
                        </td>

                        <td class="text-right company-service-widget__col-3">
                            <button class="btn btn-sm btn-default js-company-service-widget__remove" type="button">
                                <span class="sr-only">
                                    удалить
                                </span>
                                <i class="fa fa-trash" role="presentation" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                    </tbody>

                </table>


                <div class="array-controls btn-group m-t-10">
                    <button class="btn btn-sm btn-default js-company-service-widget__add" type="button">
                        <i class="fa fa-plus"></i>
                        &nbsp;
                        добавить
                    </button>

                    <button class="btn btn-sm btn-default js-company-service-widget__show-parse" type="button">
                        <i class="fa fa-file"></i>
                        &nbsp;
                        импорт из json
                    </button>
                </div>
            </div>
        </div>


        <div style="display: none;" class="js-company-service-widget__parse-box">
            <textarea class="js-parse-by-text__input" style="width: 100%; height: 400px"></textarea>
            <button class="js-parse-by-text__btn" type="button">try parse</button>
        </div>


        {{-- HINT --}}
        @if (isset($field['hint']))
            <p class="help-block">{!! $field['hint'] !!}</p>
        @endif
    </div>


    @if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
        {{-- FIELD EXTRA CSS  --}}
        {{-- push things in the after_styles section --}}

        @push('crud_fields_styles')
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
            <link href="/admin/crud/company_services/widget.css" rel="stylesheet"/>
            <!-- no styles -->
        @endpush


        {{-- FIELD EXTRA JS --}}
        {{-- push things in the after_scripts section --}}

        @push('crud_fields_scripts')
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
            <script src="/admin/crud/company_services/widget.js"></script>

            <script type="text/javascript">

            </script>
        @endpush
    @endif

</div>

{{-- Note: most of the times you'll want to use @if ($crud->checkIfFieldIsFirstOfItsType($field, $fields)) to only load CSS/JS once, even though there are multiple instances of it. --}}