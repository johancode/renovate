<a href="{{ url($crud->route . '/create' . (request()->thread ? '?thread=' . request()->thread : '') ) }}"
   class="btn btn-primary ladda-button"
   data-style="zoom-in">

    <span class="ladda-label">
        <i class="fa fa-plus"></i>
        Добавить комментайрий в обсуждение
    </span>

</a>
