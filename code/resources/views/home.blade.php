@extends('layouts.app')


@section('content')

    <div class="container">
        <div class="columns">
            <div class="column is-6 is-offset-3 page__content__bg">

                <span class="title is-4">
                    Привет «{{ auth()->getUser()->name }}» !
                </span>

            </div>
        </div>
    </div>

@endsection