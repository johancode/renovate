<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testNewUserShouldHaveActivationHash()
    {
        $user = factory(User::class)->create();
        $this->assertTrue(strlen($user->activation_hash) == 32);
    }
}
