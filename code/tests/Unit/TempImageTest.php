<?php

namespace Tests\Unit;

use App\User;
use JohanCode\BackpackImageUploader\Models\TempImage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TempImageTest extends TestCase
{
    use RefreshDatabase;

    public function testTempImageShouldBeCreated()
    {
        $testedImage = factory(TempImage::class)->create();
        $createdImage = TempImage::whereId($testedImage->id)->first();

        $this->assertEquals($testedImage->toArray(), $createdImage->toArray());
        $this->assertDatabaseHas('temp_images', ['id' => $testedImage->id]);
    }

    public function testTempImageShouldBeDeleted()
    {
        $testedImage = factory(TempImage::class)->create();
        $testedImage->delete();

        $this->assertDatabaseMissing('temp_images', ['id' => $testedImage->id]);
    }

    public function testAuthUserTempImagesShouldBeMarked()
    {
        $user = factory(User::class)->create();
        $this->be($user);

        $testedImage = factory(TempImage::class)->create();
        $this->assertDatabaseHas('temp_images', [
            'id' => $testedImage->id,
            'user_id' => $user->id
        ]);
    }
}
