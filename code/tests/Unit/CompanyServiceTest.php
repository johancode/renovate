<?php

namespace Tests\Unit;

use App\Models\CompanyService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testCompanyServiceShouldBeCreated()
    {
        $testedService = factory(CompanyService::class)->create();
        $createdService = CompanyService::whereId($testedService->id)->first();

        $this->assertEquals($testedService->toArray(), $createdService->toArray());
        $this->assertDatabaseHas('company_services', $testedService->toArray());
    }

    public function testCompanyServiceBeDeleted()
    {
        $testedService = factory(CompanyService::class)->create();
        $testedService->delete();

        $this->assertDatabaseMissing('company_services', ['id' => $testedService->id]);
    }
}
