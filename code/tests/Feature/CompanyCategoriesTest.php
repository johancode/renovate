<?php

namespace Tests\Feature;

use App\Models\CompanyCategory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyCategoriesTest extends TestCase
{
    use RefreshDatabase;

    public function testCompanyCategoryPageAvailable()
    {
        $this->get(route('companies.categories'))->assertStatus(200);

        factory(CompanyCategory::class)->create(['active' => 1]);
        $this->get(route('companies.categories'))->assertStatus(200);
    }

    public function testCompanyCategoryPageActiveStatus()
    {
        $testedService = factory(CompanyCategory::class)->create(['active' => 1]);
        $this->get($testedService->link)->assertStatus(200);

        $testedService->active = 0;
        $testedService->save();

        $this->get($testedService->link)->assertStatus(404);
    }
}
