<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserActiveStatusTest extends TestCase
{
    use RefreshDatabase;

    public function testActiveUserShouldHaveAllow()
    {
        $user = factory(User::class)->create(['active' => true]);
        $this->be($user);

        $this->get(route('home'))->assertStatus(200);
    }

    public function testNewUserShouldHaveDisallow()
    {
        $user = factory(User::class)->create();
        $this->be($user);

        $this->get(route('home'))->assertStatus(500);
    }
}
