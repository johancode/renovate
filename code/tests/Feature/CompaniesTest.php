<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\CompanyService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;


class CompaniesTest extends TestCase
{
    use RefreshDatabase;

    public function testCompaniesListPageAvailable()
    {
        $this->get(route('companies.list'))->assertStatus(200);

        $testedCompany = factory(Company::class)->create();
        $this->get(route('companies.list'))->assertStatus(200);
    }


    public function testCompanyPageActiveStatus()
    {
        $testedCompany = factory(Company::class)->create(['active' => true]);
        $this->get($testedCompany->link)->assertStatus(200);

        $testedCompany = factory(Company::class)->create(['active' => false]);
        $this->get($testedCompany->link)->assertStatus(404);
    }
}
