<?php

namespace Tests\Feature;

use App\User;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminAccessTest extends TestCase
{
    use RefreshDatabase;

    private $adminDashboardUri = null;

    public function testAdminUserShouldHaveAccess()
    {
        $user = factory(User::class)->create();
        $adminRole = Role::create(['name' => 'admin']);
        $user->assignRole($adminRole);
        $this->be($user);

        $this->get($this->adminDashboardUri)->assertStatus(200);
    }

    public function testSimpleUserShouldNotHaveAccess()
    {
        $user = factory(User::class)->create();
        $this->be($user);

        $this->get($this->adminDashboardUri)->assertStatus(401);
    }

    function setUp()
    {
        parent::setUp();

        $this->adminDashboardUri = config('backpack.base.route_prefix') . "/dashboard";
    }
}
