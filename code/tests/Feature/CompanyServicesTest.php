<?php

namespace Tests\Feature;

use App\Models\CompanyService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyServicesTest extends TestCase
{
    use RefreshDatabase;

    public function testCompanyServicesPageAvailable()
    {
        $this->get(route('companies.services'))->assertStatus(200);

        factory(CompanyService::class)->create(['active' => 1]);
        $this->get(route('companies.services'))->assertStatus(200);
    }

    public function testCompanyServicePageActiveStatus()
    {
        $testedService = factory(CompanyService::class)->create(['active' => 1]);
        $this->get($testedService->link)->assertStatus(200);

        $testedService->active = 0;
        $testedService->save();

        $this->get($testedService->link)->assertStatus(404);
    }
}
