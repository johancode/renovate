<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'johan',
            'email' => 'mihailbulin@gmail.com',
            'password' => bcrypt('boom'),
        ]);

        $adminRole = Role::create(['name' => 'admin']);

        $admin->assignRole($adminRole);
    }
}
