<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceTableActiveField extends Migration
{
    public function up()
    {
        Schema::table('company_services', function (Blueprint $table) {
            $table->boolean('active')->default(1)->index();
        });
    }

    public function down()
    {
        Schema::table('company_services', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}
