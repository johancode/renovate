<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortableToMasterCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('company_master_categories', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();
        });
    }

    public function down()
    {
        Schema::table('company_master_categories', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('lft');
            $table->dropColumn('rgt');
            $table->dropColumn('depth');
        });
    }
}
