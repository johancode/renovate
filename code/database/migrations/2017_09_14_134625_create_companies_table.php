<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url')->unique()->index();
            $table->string('phone')->nullable();
            $table->text('contacts')->nullable();
            $table->text('specialization')->nullable();
            $table->text('info')->nullable();
            $table->integer('rate')->default(0)->index();
            $table->boolean('active')->default(0)->index();
            $table->text('logo')->nullable();
            $table->text('images')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
