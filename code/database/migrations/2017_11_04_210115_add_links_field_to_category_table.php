<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinksFieldToCategoryTable extends Migration
{
    public function up()
    {
        Schema::table('company_categories', function (Blueprint $table) {
            $table->integer('master_category_id')->unsigned()->nullable();
            $table->string('full_name')->index();
        });
    }

    public function down()
    {
        Schema::table('company_categories', function (Blueprint $table) {
            $table->dropColumn('master_category_id');
            $table->dropColumn('full_name');
        });
    }
}
