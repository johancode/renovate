<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentThreadsMorphRelations extends Migration
{
    public function up()
    {
        Schema::table('comment_threads', function (Blueprint $table) {
            $table->integer('commentable_id')->nullable();
            $table->string('commentable_type')->nullable();
        });
    }

    public function down()
    {
        Schema::table('comment_threads', function (Blueprint $table) {
            $table->dropColumn('commentable_id');
            $table->dropColumn('commentable_type');
        });
    }
}
