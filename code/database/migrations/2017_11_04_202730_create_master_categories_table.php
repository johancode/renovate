<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('company_master_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('url')->unique()->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('company_master_categories');
    }
}
