<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Elasticsearch\Client;

class EsAddCompaniesIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ESClient = App::make("Elasticsearch\Client");

        if ($ESClient->indices()->exists(['index' => 'companies'])) {
            $ESClient->indices()->delete(['index' => 'companies']);
        }


        $params = [
            'index' => 'companies',
            'body' => [
                'settings' => [
                    'analysis' => [
                        'analyzer' => [
                            'my_analyzer' => [
                                "filter" => [
                                    'lowercase',
                                    'russian_morphology',
                                    'english_morphology',
                                    'my_stopwords'
                                ],
                                'type' => 'custom',
                                'tokenizer' => 'standard',
                            ],
                        ],
                        'filter' => [
                            'my_stopwords' => [
                                'type' => 'stop',
                                'stopwords' => 'а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я,a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with',
                            ],
                        ],
                    ]
                ],
                'mappings' => [
                    'test' => [
                        '_source' => [
                            'enabled' => true
                        ],
                        'properties' => [
//                            'id' => [
//                                'type' => 'integer',
//                                'index' => 'not_analyzed'
//                            ],
                            'name' => [
                                'type' => 'string',
                                'analyzer' => 'my_analyzer'
                            ],
                            'info' => [
                                'type' => 'text',
                                'analyzer' => 'my_analyzer'
                            ],
//                            'phone' => [
//                                'type' => 'string',
//                                'index' => 'not_analyzed'
//                            ],
//                            'specialization' => [
//                                'type' => 'string',
//                                'analyzer' => 'my_analyzer'
//                            ],
//                            'user_rate' => [
//                                'type' => 'integer',
//                            ],
//                            'active' => [
//                                'type' => 'boolean',
//                            ],
//                            'position_index' => [
//                                'type' => 'integer',
//                            ],
//                            'position_score' => [
//                                'type' => 'integer',
//                            ],
                        ]
                    ]
                ]
            ]
        ];

        $ESClient->indices()->create($params);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $ESClient = App::make("Elasticsearch\Client");

        if (!$ESClient->indices()->exists(['index' => 'companies'])) {
            $ESClient->indices()->delete(['index' => 'companies']);
        }
    }
}
