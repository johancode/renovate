<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCommentThemesToThreadTable extends Migration
{
    public function up()
    {
        Schema::rename("comment_themes", "comment_threads");
    }

    public function down()
    {
        Schema::rename("comment_threads", "comment_themes");
    }
}
