<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldToCompanyCategoryTable extends Migration
{
    public function up()
    {
        Schema::table('company_categories', function (Blueprint $table) {
            $table->boolean('is_shop')->default(false)->index();
            $table->boolean('is_service')->default(false)->index();
        });
    }

    public function down()
    {
        Schema::table('company_categories', function (Blueprint $table) {
            $table->dropColumn('is_shop');
            $table->dropColumn('is_service');
        });
    }
}
