<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconLabelToMasterCategoryTable extends Migration
{
    public function up()
    {
        Schema::table('company_master_categories', function (Blueprint $table) {
            $table->string('icon_label')->nullable();
        });
    }

    public function down()
    {
        Schema::table('company_master_categories', function (Blueprint $table) {
            $table->dropColumn('icon_label');
        });
    }
}
