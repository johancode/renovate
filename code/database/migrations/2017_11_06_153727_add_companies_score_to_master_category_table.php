<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompaniesScoreToMasterCategoryTable extends Migration
{
    public function up()
    {
        Schema::table('company_master_categories', function (Blueprint $table) {
            $table->string('companies_score')->nullable();
        });
    }

    public function down()
    {
        Schema::table('company_master_categories', function (Blueprint $table) {
            $table->dropColumn('companies_score');
        });
    }
}
