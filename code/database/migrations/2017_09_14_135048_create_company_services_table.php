<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyServicesTable extends Migration
{
    public function up()
    {
        Schema::create('company_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('unit_type');
            $table->string('url')->unique()->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('company_services');
    }
}
