<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('company_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('url')->unique()->index();
            $table->text('info')->nullable();
            $table->boolean('active')->default(1)->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('company_categories');
    }
}
