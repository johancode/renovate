<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyCategoryInfoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_categories', function (Blueprint $table) {
            $table->renameColumn('info', 'subtitle');
        });

        Schema::table('company_categories', function (Blueprint $table) {
            $table->string('seo_title')->nullable();
            $table->text('seo_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_categories', function (Blueprint $table) {
            $table->dropColumn('seo_title');
            $table->dropColumn('seo_text');
        });

        Schema::table('company_categories', function (Blueprint $table) {
            $table->renameColumn('subtitle', 'info');
        });
    }
}
