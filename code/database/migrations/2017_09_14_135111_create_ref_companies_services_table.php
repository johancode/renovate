<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefCompaniesServicesTable extends Migration
{
    public function up()
    {
        Schema::create('ref_companies_services', function (Blueprint $table) {
            $table->integer('company_id')->unsigned()->nullable();
            $table->integer('service_id')->unsigned()->nullable();
            $table->unique(array('company_id', 'service_id'));
            $table->integer('price')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('ref_companies_services');
    }
}
