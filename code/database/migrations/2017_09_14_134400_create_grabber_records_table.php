<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrabberRecordsTable extends Migration
{
    public function up()
    {
        Schema::create('grabber_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('source')->nullable()->index();
            $table->string('type')->nullable()->index();
            $table->string('status')->nullable()->index();
            $table->string('uri')->nullable()->index();
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grabber_records');
    }
}
