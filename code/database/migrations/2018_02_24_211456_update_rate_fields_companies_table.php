<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRateFieldsCompaniesTable extends Migration
{
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn("rate", "user_rate");
            $table->integer('position_score')->default(0);
        });
    }

    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->renameColumn("user_rate", "rate");
            $table->dropColumn('position_score');
        });
    }
}
