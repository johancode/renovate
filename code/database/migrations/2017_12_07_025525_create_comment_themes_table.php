<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentThemesTable extends Migration
{
    public function up()
    {
        Schema::create('comment_themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('comment_themes');
    }
}
