<?php

use Faker\Generator as Faker;

$factory->define(\JohanCode\BackpackImageUploader\Models\TempImage::class, function (Faker $faker) {
    return [
        'file' => null,
        'user_id' => 1,
    ];
});
