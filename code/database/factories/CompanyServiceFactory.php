<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\CompanyService::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'unit_type' => "кв метр",
        'active' => $faker->numberBetween(0, 1),
    ];
});
