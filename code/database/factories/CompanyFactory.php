<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'phone' => $faker->tollFreePhoneNumber,
        'contacts' => [
            [
                'type' => 'site',
                'value' => $faker->url
            ],
            [
                'type' => 'site',
                'value' => $faker->url
            ],
            [
                'type' => 'phone',
                'value' => $faker->phoneNumber
            ],
        ],
        'specialization' => $faker->paragraph,
        'info' => $faker->text,
        'rate' => $faker->numberBetween(0, 5),
        'active' => $faker->numberBetween(0, 1),
        'logo' => null,
        'images' => null,
        'addresses' => null,
        'schedule' => null,
        'source' => 0
    ];
});
