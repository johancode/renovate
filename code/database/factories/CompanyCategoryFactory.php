<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\CompanyCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'info' => $faker->text,
        'active' => $faker->numberBetween(0, 1),
    ];
});
