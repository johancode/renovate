#!/bin/bash


ROOTDIR=$(pwd)

cd $ROOTDIR
git pull

echo "=> site down"
cd $ROOTDIR
docker-compose exec renovate_php php /app/artisan down


echo "=> migration"
cd $ROOTDIR
docker-compose exec renovate_php php /app/artisan migrate --force

echo "=> composer install"
cd $ROOTDIR
docker run --rm -v $(pwd)/code:/app composer install --ignore-platform-reqs

echo "=> yarn install"
cd $ROOTDIR
docker-compose run --rm renovate_frontend yarn install
docker-compose run --rm renovate_frontend webpack -p

echo "=> restart queue"
cd $ROOTDIR
docker-compose exec renovate_php php /app/artisan queue:restart

echo "=> site up"
cd $ROOTDIR
docker-compose exec renovate_php php /app/artisan up